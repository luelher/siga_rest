<?php
namespace Controllers\Compras;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Controllers\Compras;
use \Utils\H;
use \Business\Compras\Recepcion;
use \CasolartQuery;
use \CaartordQuery;
use \CaordcomQuery;
use \CaregartQuery;
use \Carcpart;
use \Caartord;
use \CaartordPeer;
use \Criteria;


class ComprasControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];;

        // Create Presupuesto Base
        $controllers->post('/recepcion_orden', 'Controllers\Compras\ComprasControllerProvider::recepcion_orden');
        $controllers->get('/solicitud_egreso/{solegr}', 'Controllers\Compras\ComprasControllerProvider::solicitud_egreso_find');
        $controllers->get('/solicitud_egreso', 'Controllers\Compras\ComprasControllerProvider::solicitud_egreso_find');
        // $controllers->get('/cliente', 'Controllers\Facturacion\ClienteControllerProvider::cliente_find');

        return $controllers;
    }


    public function solicitud_egreso_find(Application $app, Request $request){
      $solegr = $request->get('solegr');
      if($solegr==null){
        $casolart = CasolartQuery::create()->find()->toArray();
      }else{
        $casolart = CasolartQuery::create()->findOneByReqart($solegr);
        if($casolart){
          $caordcom = $casolart->getCaordcom();
          $casolart = $casolart->toArray();
          if($caordcom){
            $casolart['Ordcom'] = $caordcom->getOrdcom();
          }else $casolart['Ordcom'] = '';
        } 
      }
      $resp = array("response" => array('cod' => '-1'), 'data' => $casolart);
      return new Response(json_encode($resp), Response::HTTP_OK);
    }


    public function recepcion_orden(Application $app, Request $request) {

      $carcpart = $request->get('carcpart');
      $caartord = $request->get('caartord');

      $valid = Recepcion::ValidarRecepcionDesdeArray($carcpart, $caartord);
      if($valid){

        $carcpart_obj = new Carcpart();
        $carcpart_obj->fromArray($carcpart, \BasePeer::TYPE_FIELDNAME);
        $caordcom_obj = CaordcomQuery::create()->findOneByOrdcom($carcpart['ordcom']);

        //if($caordcom_obj){          
        //  $carcpart_obj->setCodpro($caordcom_obj->getCodpro());
        //}
        //else {
            if ($carcpart['fecord']=="")
                $sql = "Select codpro,codcat from caordenesview where ordcom='".$carcpart['ordcom']."' order by fecord desc limit 1";
            else
                $sql = "Select codpro,codcat from caordenesview where ordcom='".$carcpart['ordcom']."' and fecord='".$carcpart['fecord']."' order by fecord desc limit 1";  
            if (H::BuscarDatos($sql,$orden))
               $carcpart_obj->setCodpro($orden['codpro']);

        //}
        $caartord_array = array();
        foreach ($caartord as $v) {

            $articulo = CaregartQuery::create()->findOneByCodart($v["codart"]);
            if ($articulo)
              $elmonto=(float)$v["canrecgri"]*$articulo->getCospro();
            else
              $elmonto=(float)$v["montot"];

            $caartord_obj = new \Caartord();
            $caartord_obj->fromArray($v, \BasePeer::TYPE_FIELDNAME);
            $caartord_obj->setCanrecgri((float)$v["canrecgri"]);
            $caartord_obj->setCandev(0);
            $caartord_obj->setCodalm($v["codalm"]);
            $caartord_obj->setCodubi($v["codubi"]);
            $caartord_obj->setCodart($v["codart"]);
            $caartord_obj->setOrdcom($carcpart_obj->getOrdcom());
            $caartord_obj->setCanord((float)$v["canrecgri"]);
            $caartord_obj->setMontot($elmonto);
            $caartord_obj->setCodcat($v["codcat"]);
            $caartord_array[] = $caartord_obj;
        }
        

          
            /*$t= new Criteria();
            $t->add(CaartordPeer::ORDCOM,$carcpart_obj->getOrdcom());
            $t->add(CaartordPeer::CODART,$v["codart"]);
            $caartord_obj=CaartordPeer::doSelectOne($t);
            $caartord_1 = CaartordQuery::create()->findOneByOrdcom($carcpart_obj->getOrdcom());
            //print_r($caartord_1);    exit();
            if($caartord_1){
              foreach ($caartord_1 as $x) {
                foreach ($caartord as $v) {
                  if ($v["codart"]==$x->getCodart()){
                     $caartord_obj = new \Caartord();
                      $caartord_obj=$x;
                    $caartord_obj->setCanrecgri((float)$v["canrecgri"]);
                    //$caartord_obj->setCanord((float)$v["canrecgri"]);
                    //$caartord_obj->setCandev(0);
                    $caartord_obj->setCodalm($v["codalm"]);
                    $caartord_obj->setCodubi($v["codubi"]);
                    //$caartord_obj->setCodart($v["codart"]);
                    //$caartord_obj->setOrdcom($carcpart_obj->getOrdcom());
                    $caartord_array[] = $caartord_obj;
                  }
                }
               
            }
            } */
          


        /*foreach ($caartord as $v) {
          print $carcpart_obj->getOrdcom().'r'.$v["codart"];
          $caartord_obj = CaartordQuery::create()->filterByOrdcom($carcpart_obj->getOrdcom())->filterByCodart($v["codart"])->findOne();
          print_r($caartord_obj); exit;
          if($caartord_obj){
            $caartord_obj->setCanrecgri((float)$v["canrecgri"]);
            $caartord_obj->setCandev(0);
            $caartord_obj->setCodalm($v["codalm"]);
            $caartord_obj->setCodubi($v["codubi"]);

            $caartord_array[] = $caartord_obj;
          } 
        }*/

        //print_r($caartord_array); exit;
        
        try{
          $error = Recepcion::salvarAlmrec($carcpart_obj,array($caartord_array));
          if($error == -1){
            $resp = array("response" => array("cod" => '-1', "msj" => "Articulos Recibidos"), "data" => array("carcpart" => $carcpart_obj->toArray()));
          }else{
            $resp = array("response" => H::obtenerMensajeError($error, true), "data" => array($carcpart_obj->toArray()));
          }            
        }catch (Exception $e){
          $resp = array("response" => H::obtenerMensajeError("0", true), "data" => "Excepción");
        }
      }else $resp = array("response" => H::obtenerMensajeError("0", true), "data" => "Validacion de Datos");

      return new Response(json_encode($resp), Response::HTTP_CREATED);
    }



}
?>
