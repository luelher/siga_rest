Instalacion
========

git clone https://bitbucket.org/luelher/siga_rest.git

cd siga_rest

apt-get install curl

curl -sS https://getcomposer.org/installer | php

./composer.phar update

php -S localhost:8000 -t public



Configurar En Apache
==================

Crear en public el archivo .htaccess y agregar...

<IfModule mod_rewrite.c>
    Options -MultiViews

    RewriteEngine On
    #RewriteBase /path/to/app
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [QSA,L]
</IfModule>

habilitar el rewrite en apache

a2ensite rewrite

reiniciar apache


Listo!!!!!!