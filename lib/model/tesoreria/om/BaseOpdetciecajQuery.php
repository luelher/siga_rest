<?php


/**
 * Base class that represents a query for the 'opdetciecaj' table.
 *
 * Tabla que contiene información referente al detalle del Cierre de Caja Chica
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:43 2015
 *
 * @method OpdetciecajQuery orderByNumref($order = Criteria::ASC) Order by the numref column
 * @method OpdetciecajQuery orderByCodpre($order = Criteria::ASC) Order by the codpre column
 * @method OpdetciecajQuery orderByMoncom($order = Criteria::ASC) Order by the moncom column
 * @method OpdetciecajQuery orderByRefsal($order = Criteria::ASC) Order by the refsal column
 * @method OpdetciecajQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method OpdetciecajQuery groupByNumref() Group by the numref column
 * @method OpdetciecajQuery groupByCodpre() Group by the codpre column
 * @method OpdetciecajQuery groupByMoncom() Group by the moncom column
 * @method OpdetciecajQuery groupByRefsal() Group by the refsal column
 * @method OpdetciecajQuery groupById() Group by the id column
 *
 * @method OpdetciecajQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method OpdetciecajQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method OpdetciecajQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Opdetciecaj findOne(PropelPDO $con = null) Return the first Opdetciecaj matching the query
 * @method Opdetciecaj findOneOrCreate(PropelPDO $con = null) Return the first Opdetciecaj matching the query, or a new Opdetciecaj object populated from the query conditions when no match is found
 *
 * @method Opdetciecaj findOneByNumref(string $numref) Return the first Opdetciecaj filtered by the numref column
 * @method Opdetciecaj findOneByCodpre(string $codpre) Return the first Opdetciecaj filtered by the codpre column
 * @method Opdetciecaj findOneByMoncom(string $moncom) Return the first Opdetciecaj filtered by the moncom column
 * @method Opdetciecaj findOneByRefsal(string $refsal) Return the first Opdetciecaj filtered by the refsal column
 *
 * @method array findByNumref(string $numref) Return Opdetciecaj objects filtered by the numref column
 * @method array findByCodpre(string $codpre) Return Opdetciecaj objects filtered by the codpre column
 * @method array findByMoncom(string $moncom) Return Opdetciecaj objects filtered by the moncom column
 * @method array findByRefsal(string $refsal) Return Opdetciecaj objects filtered by the refsal column
 * @method array findById(int $id) Return Opdetciecaj objects filtered by the id column
 *
 * @package    propel.generator.lib.model.tesoreria.om
 */
abstract class BaseOpdetciecajQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseOpdetciecajQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Opdetciecaj', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new OpdetciecajQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   OpdetciecajQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return OpdetciecajQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof OpdetciecajQuery) {
            return $criteria;
        }
        $query = new OpdetciecajQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Opdetciecaj|Opdetciecaj[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = OpdetciecajPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(OpdetciecajPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Opdetciecaj A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Opdetciecaj A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numref", "codpre", "moncom", "refsal", "id" FROM "opdetciecaj" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Opdetciecaj();
            $obj->hydrate($row);
            OpdetciecajPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Opdetciecaj|Opdetciecaj[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Opdetciecaj[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OpdetciecajPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OpdetciecajPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numref column
     *
     * Example usage:
     * <code>
     * $query->filterByNumref('fooValue');   // WHERE numref = 'fooValue'
     * $query->filterByNumref('%fooValue%'); // WHERE numref LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numref The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterByNumref($numref = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numref)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numref)) {
                $numref = str_replace('*', '%', $numref);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpdetciecajPeer::NUMREF, $numref, $comparison);
    }

    /**
     * Filter the query on the codpre column
     *
     * Example usage:
     * <code>
     * $query->filterByCodpre('fooValue');   // WHERE codpre = 'fooValue'
     * $query->filterByCodpre('%fooValue%'); // WHERE codpre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codpre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterByCodpre($codpre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codpre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codpre)) {
                $codpre = str_replace('*', '%', $codpre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpdetciecajPeer::CODPRE, $codpre, $comparison);
    }

    /**
     * Filter the query on the moncom column
     *
     * Example usage:
     * <code>
     * $query->filterByMoncom(1234); // WHERE moncom = 1234
     * $query->filterByMoncom(array(12, 34)); // WHERE moncom IN (12, 34)
     * $query->filterByMoncom(array('min' => 12)); // WHERE moncom >= 12
     * $query->filterByMoncom(array('max' => 12)); // WHERE moncom <= 12
     * </code>
     *
     * @param     mixed $moncom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterByMoncom($moncom = null, $comparison = null)
    {
        if (is_array($moncom)) {
            $useMinMax = false;
            if (isset($moncom['min'])) {
                $this->addUsingAlias(OpdetciecajPeer::MONCOM, $moncom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moncom['max'])) {
                $this->addUsingAlias(OpdetciecajPeer::MONCOM, $moncom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpdetciecajPeer::MONCOM, $moncom, $comparison);
    }

    /**
     * Filter the query on the refsal column
     *
     * Example usage:
     * <code>
     * $query->filterByRefsal('fooValue');   // WHERE refsal = 'fooValue'
     * $query->filterByRefsal('%fooValue%'); // WHERE refsal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refsal The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterByRefsal($refsal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refsal)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refsal)) {
                $refsal = str_replace('*', '%', $refsal);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OpdetciecajPeer::REFSAL, $refsal, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OpdetciecajPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OpdetciecajPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OpdetciecajPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Opdetciecaj $opdetciecaj Object to remove from the list of results
     *
     * @return OpdetciecajQuery The current query, for fluid interface
     */
    public function prune($opdetciecaj = null)
    {
        if ($opdetciecaj) {
            $this->addUsingAlias(OpdetciecajPeer::ID, $opdetciecaj->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
