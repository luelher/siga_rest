<?php



/**
 * This class defines the structure of the 'tsrelfonvia' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:42 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.tesoreria.map
 */
class TsrelfonviaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.tesoreria.map.TsrelfonviaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tsrelfonvia');
        $this->setPhpName('Tsrelfonvia');
        $this->setClassname('Tsrelfonvia');
        $this->setPackage('lib.model.tesoreria');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('numsol', 'Numsol', 'VARCHAR', true, 10, null);
        $this->addColumn('numche', 'Numche', 'VARCHAR', true, 20, null);
        $this->addColumn('numcue', 'Numcue', 'VARCHAR', true, 20, null);
        $this->addColumn('cedrif', 'Cedrif', 'VARCHAR', false, 15, null);
        $this->addColumn('nomben', 'Nomben', 'VARCHAR', false, 100, null);
        $this->addColumn('monche', 'Monche', 'NUMERIC', false, 14, null);
        $this->addColumn('codcat', 'Codcat', 'VARCHAR', false, 32, null);
        $this->addColumn('fecemi', 'Fecemi', 'DATE', false, null, null);
        $this->addColumn('codpre', 'Codpre', 'VARCHAR', false, 50, null);
        $this->addColumn('numdep', 'Numdep', 'VARCHAR', false, 20, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // TsrelfonviaTableMap
