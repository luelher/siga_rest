<?php



/**
 * This class defines the structure of the 'tsrepret' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:42 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.tesoreria.map
 */
class TsrepretTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.tesoreria.map.TsrepretTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tsrepret');
        $this->setPhpName('Tsrepret');
        $this->setClassname('Tsrepret');
        $this->setPackage('lib.model.tesoreria');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('codrep', 'Codrep', 'VARCHAR', true, 50, null);
        $this->addForeignKey('codret', 'Codret', 'VARCHAR', 'optipret', 'codtip', true, 4, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Optipret', 'Optipret', RelationMap::MANY_TO_ONE, array('codret' => 'codtip', ), null, null);
    } // buildRelations()

} // TsrepretTableMap
