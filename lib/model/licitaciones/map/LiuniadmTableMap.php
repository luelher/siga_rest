<?php



/**
 * This class defines the structure of the 'liuniadm' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:52 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.licitaciones.map
 */
class LiuniadmTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.licitaciones.map.LiuniadmTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('liuniadm');
        $this->setPhpName('Liuniadm');
        $this->setClassname('Liuniadm');
        $this->setPackage('lib.model.licitaciones');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('coduniadm', 'Coduniadm', 'VARCHAR', false, 3, null);
        $this->addColumn('desuniadm', 'Desuniadm', 'VARCHAR', false, 100, null);
        $this->addColumn('codemp', 'Codemp', 'VARCHAR', false, 16, null);
        $this->addColumn('nomemp', 'Nomemp', 'VARCHAR', false, 100, null);
        $this->addColumn('nomcar', 'Nomcar', 'VARCHAR', false, 100, null);
        $this->addColumn('resolu', 'Resolu', 'VARCHAR', false, 20, null);
        $this->addColumn('fecres', 'Fecres', 'DATE', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Lidetcroentcont', 'Lidetcroentcont', RelationMap::ONE_TO_MANY, array('coduniadm' => 'coduniadm', ), null, null, 'Lidetcroentconts');
        $this->addRelation('Lidetcroentcontob', 'Lidetcroentcontob', RelationMap::ONE_TO_MANY, array('coduniadm' => 'coduniadm', ), null, null, 'Lidetcroentcontobs');
        $this->addRelation('Lidetcroentmodcont', 'Lidetcroentmodcont', RelationMap::ONE_TO_MANY, array('coduniadm' => 'coduniadm', ), null, null, 'Lidetcroentmodconts');
        $this->addRelation('Lidetcroentaddcont', 'Lidetcroentaddcont', RelationMap::ONE_TO_MANY, array('coduniadm' => 'coduniadm', ), null, null, 'Lidetcroentaddconts');
    } // buildRelations()

} // LiuniadmTableMap
