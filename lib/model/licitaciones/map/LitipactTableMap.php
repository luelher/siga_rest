<?php



/**
 * This class defines the structure of the 'litipact' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:54 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.licitaciones.map
 */
class LitipactTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.licitaciones.map.LitipactTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('litipact');
        $this->setPhpName('Litipact');
        $this->setClassname('Litipact');
        $this->setPackage('lib.model.licitaciones');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('codtipact', 'Codtipact', 'VARCHAR', false, 8, null);
        $this->addColumn('nomtipact', 'Nomtipact', 'VARCHAR', false, 100, null);
        $this->addColumn('dettipact', 'Dettipact', 'VARCHAR', false, 10000, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Liactas', 'Liactas', RelationMap::ONE_TO_MANY, array('codtipact' => 'codtipact', ), null, null, 'Liactass');
    } // buildRelations()

} // LitipactTableMap
