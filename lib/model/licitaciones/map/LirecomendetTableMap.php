<?php



/**
 * This class defines the structure of the 'lirecomendet' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:52 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.licitaciones.map
 */
class LirecomendetTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.licitaciones.map.LirecomendetTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('lirecomendet');
        $this->setPhpName('Lirecomendet');
        $this->setClassname('Lirecomendet');
        $this->setPackage('lib.model.licitaciones');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('numrecofe', 'Numrecofe', 'VARCHAR', false, 8, null);
        $this->addColumn('codpro', 'Codpro', 'VARCHAR', false, 15, null);
        $this->addColumn('punleg', 'Punleg', 'NUMERIC', false, 14, null);
        $this->addColumn('puntec', 'Puntec', 'NUMERIC', false, 14, null);
        $this->addColumn('punfin', 'Punfin', 'NUMERIC', false, 14, null);
        $this->addColumn('punfia', 'Punfia', 'NUMERIC', false, 14, null);
        $this->addColumn('puntipemp', 'Puntipemp', 'NUMERIC', false, 14, null);
        $this->addColumn('punvan', 'Punvan', 'NUMERIC', false, 14, null);
        $this->addColumn('punmin', 'Punmin', 'NUMERIC', false, 14, null);
        $this->addColumn('puntot', 'Puntot', 'NUMERIC', false, 14, null);
        $this->addColumn('tipconpub', 'Tipconpub', 'VARCHAR', false, 1, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // LirecomendetTableMap
