<?php


/**
 * Base class that represents a query for the 'contabc' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:39 2015
 *
 * @method ContabcQuery orderByNumcom($order = Criteria::ASC) Order by the numcom column
 * @method ContabcQuery orderByFeccom($order = Criteria::ASC) Order by the feccom column
 * @method ContabcQuery orderByDescom($order = Criteria::ASC) Order by the descom column
 * @method ContabcQuery orderByMoncom($order = Criteria::ASC) Order by the moncom column
 * @method ContabcQuery orderByStacom($order = Criteria::ASC) Order by the stacom column
 * @method ContabcQuery orderByTipcom($order = Criteria::ASC) Order by the tipcom column
 * @method ContabcQuery orderByReftra($order = Criteria::ASC) Order by the reftra column
 * @method ContabcQuery orderByLoguse($order = Criteria::ASC) Order by the loguse column
 * @method ContabcQuery orderByUsuanu($order = Criteria::ASC) Order by the usuanu column
 * @method ContabcQuery orderByCodtiptra($order = Criteria::ASC) Order by the codtiptra column
 * @method ContabcQuery orderByStaapr($order = Criteria::ASC) Order by the staapr column
 * @method ContabcQuery orderByFecapr($order = Criteria::ASC) Order by the fecapr column
 * @method ContabcQuery orderByUsuapr($order = Criteria::ASC) Order by the usuapr column
 * @method ContabcQuery orderByCoddirec($order = Criteria::ASC) Order by the coddirec column
 * @method ContabcQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method ContabcQuery groupByNumcom() Group by the numcom column
 * @method ContabcQuery groupByFeccom() Group by the feccom column
 * @method ContabcQuery groupByDescom() Group by the descom column
 * @method ContabcQuery groupByMoncom() Group by the moncom column
 * @method ContabcQuery groupByStacom() Group by the stacom column
 * @method ContabcQuery groupByTipcom() Group by the tipcom column
 * @method ContabcQuery groupByReftra() Group by the reftra column
 * @method ContabcQuery groupByLoguse() Group by the loguse column
 * @method ContabcQuery groupByUsuanu() Group by the usuanu column
 * @method ContabcQuery groupByCodtiptra() Group by the codtiptra column
 * @method ContabcQuery groupByStaapr() Group by the staapr column
 * @method ContabcQuery groupByFecapr() Group by the fecapr column
 * @method ContabcQuery groupByUsuapr() Group by the usuapr column
 * @method ContabcQuery groupByCoddirec() Group by the coddirec column
 * @method ContabcQuery groupById() Group by the id column
 *
 * @method ContabcQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ContabcQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ContabcQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ContabcQuery leftJoinContabc1($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contabc1 relation
 * @method ContabcQuery rightJoinContabc1($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contabc1 relation
 * @method ContabcQuery innerJoinContabc1($relationAlias = null) Adds a INNER JOIN clause to the query using the Contabc1 relation
 *
 * @method Contabc findOne(PropelPDO $con = null) Return the first Contabc matching the query
 * @method Contabc findOneOrCreate(PropelPDO $con = null) Return the first Contabc matching the query, or a new Contabc object populated from the query conditions when no match is found
 *
 * @method Contabc findOneByNumcom(string $numcom) Return the first Contabc filtered by the numcom column
 * @method Contabc findOneByFeccom(string $feccom) Return the first Contabc filtered by the feccom column
 * @method Contabc findOneByDescom(string $descom) Return the first Contabc filtered by the descom column
 * @method Contabc findOneByMoncom(string $moncom) Return the first Contabc filtered by the moncom column
 * @method Contabc findOneByStacom(string $stacom) Return the first Contabc filtered by the stacom column
 * @method Contabc findOneByTipcom(string $tipcom) Return the first Contabc filtered by the tipcom column
 * @method Contabc findOneByReftra(string $reftra) Return the first Contabc filtered by the reftra column
 * @method Contabc findOneByLoguse(string $loguse) Return the first Contabc filtered by the loguse column
 * @method Contabc findOneByUsuanu(string $usuanu) Return the first Contabc filtered by the usuanu column
 * @method Contabc findOneByCodtiptra(string $codtiptra) Return the first Contabc filtered by the codtiptra column
 * @method Contabc findOneByStaapr(string $staapr) Return the first Contabc filtered by the staapr column
 * @method Contabc findOneByFecapr(string $fecapr) Return the first Contabc filtered by the fecapr column
 * @method Contabc findOneByUsuapr(string $usuapr) Return the first Contabc filtered by the usuapr column
 * @method Contabc findOneByCoddirec(string $coddirec) Return the first Contabc filtered by the coddirec column
 *
 * @method array findByNumcom(string $numcom) Return Contabc objects filtered by the numcom column
 * @method array findByFeccom(string $feccom) Return Contabc objects filtered by the feccom column
 * @method array findByDescom(string $descom) Return Contabc objects filtered by the descom column
 * @method array findByMoncom(string $moncom) Return Contabc objects filtered by the moncom column
 * @method array findByStacom(string $stacom) Return Contabc objects filtered by the stacom column
 * @method array findByTipcom(string $tipcom) Return Contabc objects filtered by the tipcom column
 * @method array findByReftra(string $reftra) Return Contabc objects filtered by the reftra column
 * @method array findByLoguse(string $loguse) Return Contabc objects filtered by the loguse column
 * @method array findByUsuanu(string $usuanu) Return Contabc objects filtered by the usuanu column
 * @method array findByCodtiptra(string $codtiptra) Return Contabc objects filtered by the codtiptra column
 * @method array findByStaapr(string $staapr) Return Contabc objects filtered by the staapr column
 * @method array findByFecapr(string $fecapr) Return Contabc objects filtered by the fecapr column
 * @method array findByUsuapr(string $usuapr) Return Contabc objects filtered by the usuapr column
 * @method array findByCoddirec(string $coddirec) Return Contabc objects filtered by the coddirec column
 * @method array findById(int $id) Return Contabc objects filtered by the id column
 *
 * @package    propel.generator.lib.model.contabilidad.om
 */
abstract class BaseContabcQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseContabcQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Contabc', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ContabcQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ContabcQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ContabcQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ContabcQuery) {
            return $criteria;
        }
        $query = new ContabcQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Contabc|Contabc[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ContabcPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ContabcPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Contabc A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Contabc A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numcom", "feccom", "descom", "moncom", "stacom", "tipcom", "reftra", "loguse", "usuanu", "codtiptra", "staapr", "fecapr", "usuapr", "coddirec", "id" FROM "contabc" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Contabc();
            $obj->hydrate($row);
            ContabcPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Contabc|Contabc[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Contabc[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContabcPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContabcPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numcom column
     *
     * Example usage:
     * <code>
     * $query->filterByNumcom('fooValue');   // WHERE numcom = 'fooValue'
     * $query->filterByNumcom('%fooValue%'); // WHERE numcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByNumcom($numcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numcom)) {
                $numcom = str_replace('*', '%', $numcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::NUMCOM, $numcom, $comparison);
    }

    /**
     * Filter the query on the feccom column
     *
     * Example usage:
     * <code>
     * $query->filterByFeccom('2011-03-14'); // WHERE feccom = '2011-03-14'
     * $query->filterByFeccom('now'); // WHERE feccom = '2011-03-14'
     * $query->filterByFeccom(array('max' => 'yesterday')); // WHERE feccom > '2011-03-13'
     * </code>
     *
     * @param     mixed $feccom The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByFeccom($feccom = null, $comparison = null)
    {
        if (is_array($feccom)) {
            $useMinMax = false;
            if (isset($feccom['min'])) {
                $this->addUsingAlias(ContabcPeer::FECCOM, $feccom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($feccom['max'])) {
                $this->addUsingAlias(ContabcPeer::FECCOM, $feccom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContabcPeer::FECCOM, $feccom, $comparison);
    }

    /**
     * Filter the query on the descom column
     *
     * Example usage:
     * <code>
     * $query->filterByDescom('fooValue');   // WHERE descom = 'fooValue'
     * $query->filterByDescom('%fooValue%'); // WHERE descom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByDescom($descom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descom)) {
                $descom = str_replace('*', '%', $descom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::DESCOM, $descom, $comparison);
    }

    /**
     * Filter the query on the moncom column
     *
     * Example usage:
     * <code>
     * $query->filterByMoncom(1234); // WHERE moncom = 1234
     * $query->filterByMoncom(array(12, 34)); // WHERE moncom IN (12, 34)
     * $query->filterByMoncom(array('min' => 12)); // WHERE moncom >= 12
     * $query->filterByMoncom(array('max' => 12)); // WHERE moncom <= 12
     * </code>
     *
     * @param     mixed $moncom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByMoncom($moncom = null, $comparison = null)
    {
        if (is_array($moncom)) {
            $useMinMax = false;
            if (isset($moncom['min'])) {
                $this->addUsingAlias(ContabcPeer::MONCOM, $moncom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moncom['max'])) {
                $this->addUsingAlias(ContabcPeer::MONCOM, $moncom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContabcPeer::MONCOM, $moncom, $comparison);
    }

    /**
     * Filter the query on the stacom column
     *
     * Example usage:
     * <code>
     * $query->filterByStacom('fooValue');   // WHERE stacom = 'fooValue'
     * $query->filterByStacom('%fooValue%'); // WHERE stacom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stacom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByStacom($stacom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stacom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $stacom)) {
                $stacom = str_replace('*', '%', $stacom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::STACOM, $stacom, $comparison);
    }

    /**
     * Filter the query on the tipcom column
     *
     * Example usage:
     * <code>
     * $query->filterByTipcom('fooValue');   // WHERE tipcom = 'fooValue'
     * $query->filterByTipcom('%fooValue%'); // WHERE tipcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByTipcom($tipcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipcom)) {
                $tipcom = str_replace('*', '%', $tipcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::TIPCOM, $tipcom, $comparison);
    }

    /**
     * Filter the query on the reftra column
     *
     * Example usage:
     * <code>
     * $query->filterByReftra('fooValue');   // WHERE reftra = 'fooValue'
     * $query->filterByReftra('%fooValue%'); // WHERE reftra LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reftra The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByReftra($reftra = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reftra)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reftra)) {
                $reftra = str_replace('*', '%', $reftra);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::REFTRA, $reftra, $comparison);
    }

    /**
     * Filter the query on the loguse column
     *
     * Example usage:
     * <code>
     * $query->filterByLoguse('fooValue');   // WHERE loguse = 'fooValue'
     * $query->filterByLoguse('%fooValue%'); // WHERE loguse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $loguse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByLoguse($loguse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($loguse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $loguse)) {
                $loguse = str_replace('*', '%', $loguse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::LOGUSE, $loguse, $comparison);
    }

    /**
     * Filter the query on the usuanu column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuanu('fooValue');   // WHERE usuanu = 'fooValue'
     * $query->filterByUsuanu('%fooValue%'); // WHERE usuanu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usuanu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByUsuanu($usuanu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usuanu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $usuanu)) {
                $usuanu = str_replace('*', '%', $usuanu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::USUANU, $usuanu, $comparison);
    }

    /**
     * Filter the query on the codtiptra column
     *
     * Example usage:
     * <code>
     * $query->filterByCodtiptra('fooValue');   // WHERE codtiptra = 'fooValue'
     * $query->filterByCodtiptra('%fooValue%'); // WHERE codtiptra LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codtiptra The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByCodtiptra($codtiptra = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codtiptra)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codtiptra)) {
                $codtiptra = str_replace('*', '%', $codtiptra);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::CODTIPTRA, $codtiptra, $comparison);
    }

    /**
     * Filter the query on the staapr column
     *
     * Example usage:
     * <code>
     * $query->filterByStaapr('fooValue');   // WHERE staapr = 'fooValue'
     * $query->filterByStaapr('%fooValue%'); // WHERE staapr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $staapr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByStaapr($staapr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($staapr)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $staapr)) {
                $staapr = str_replace('*', '%', $staapr);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::STAAPR, $staapr, $comparison);
    }

    /**
     * Filter the query on the fecapr column
     *
     * Example usage:
     * <code>
     * $query->filterByFecapr('2011-03-14'); // WHERE fecapr = '2011-03-14'
     * $query->filterByFecapr('now'); // WHERE fecapr = '2011-03-14'
     * $query->filterByFecapr(array('max' => 'yesterday')); // WHERE fecapr > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecapr The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByFecapr($fecapr = null, $comparison = null)
    {
        if (is_array($fecapr)) {
            $useMinMax = false;
            if (isset($fecapr['min'])) {
                $this->addUsingAlias(ContabcPeer::FECAPR, $fecapr['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecapr['max'])) {
                $this->addUsingAlias(ContabcPeer::FECAPR, $fecapr['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContabcPeer::FECAPR, $fecapr, $comparison);
    }

    /**
     * Filter the query on the usuapr column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuapr('fooValue');   // WHERE usuapr = 'fooValue'
     * $query->filterByUsuapr('%fooValue%'); // WHERE usuapr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usuapr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByUsuapr($usuapr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usuapr)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $usuapr)) {
                $usuapr = str_replace('*', '%', $usuapr);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::USUAPR, $usuapr, $comparison);
    }

    /**
     * Filter the query on the coddirec column
     *
     * Example usage:
     * <code>
     * $query->filterByCoddirec('fooValue');   // WHERE coddirec = 'fooValue'
     * $query->filterByCoddirec('%fooValue%'); // WHERE coddirec LIKE '%fooValue%'
     * </code>
     *
     * @param     string $coddirec The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterByCoddirec($coddirec = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($coddirec)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $coddirec)) {
                $coddirec = str_replace('*', '%', $coddirec);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ContabcPeer::CODDIREC, $coddirec, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ContabcPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ContabcPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContabcPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Contabc1 object
     *
     * @param   Contabc1|PropelObjectCollection $contabc1  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ContabcQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByContabc1($contabc1, $comparison = null)
    {
        if ($contabc1 instanceof Contabc1) {
            return $this
                ->addUsingAlias(ContabcPeer::NUMCOM, $contabc1->getNumcom(), $comparison);
        } elseif ($contabc1 instanceof PropelObjectCollection) {
            return $this
                ->useContabc1Query()
                ->filterByPrimaryKeys($contabc1->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContabc1() only accepts arguments of type Contabc1 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contabc1 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function joinContabc1($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contabc1');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contabc1');
        }

        return $this;
    }

    /**
     * Use the Contabc1 relation Contabc1 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   Contabc1Query A secondary query class using the current class as primary query
     */
    public function useContabc1Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContabc1($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contabc1', 'Contabc1Query');
    }

    /**
     * Exclude object from result
     *
     * @param   Contabc $contabc Object to remove from the list of results
     *
     * @return ContabcQuery The current query, for fluid interface
     */
    public function prune($contabc = null)
    {
        if ($contabc) {
            $this->addUsingAlias(ContabcPeer::ID, $contabc->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
