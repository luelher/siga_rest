<?php


/**
 * Base class that represents a query for the 'contabc1' table.
 *
 * Detalle de los comprobantes contables
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:40 2015
 *
 * @method Contabc1Query orderByNumcom($order = Criteria::ASC) Order by the numcom column
 * @method Contabc1Query orderByFeccom($order = Criteria::ASC) Order by the feccom column
 * @method Contabc1Query orderByDebcre($order = Criteria::ASC) Order by the debcre column
 * @method Contabc1Query orderByCodcta($order = Criteria::ASC) Order by the codcta column
 * @method Contabc1Query orderByNumasi($order = Criteria::ASC) Order by the numasi column
 * @method Contabc1Query orderByRefasi($order = Criteria::ASC) Order by the refasi column
 * @method Contabc1Query orderByDesasi($order = Criteria::ASC) Order by the desasi column
 * @method Contabc1Query orderByMonasi($order = Criteria::ASC) Order by the monasi column
 * @method Contabc1Query orderByCodcencos($order = Criteria::ASC) Order by the codcencos column
 * @method Contabc1Query orderById($order = Criteria::ASC) Order by the id column
 *
 * @method Contabc1Query groupByNumcom() Group by the numcom column
 * @method Contabc1Query groupByFeccom() Group by the feccom column
 * @method Contabc1Query groupByDebcre() Group by the debcre column
 * @method Contabc1Query groupByCodcta() Group by the codcta column
 * @method Contabc1Query groupByNumasi() Group by the numasi column
 * @method Contabc1Query groupByRefasi() Group by the refasi column
 * @method Contabc1Query groupByDesasi() Group by the desasi column
 * @method Contabc1Query groupByMonasi() Group by the monasi column
 * @method Contabc1Query groupByCodcencos() Group by the codcencos column
 * @method Contabc1Query groupById() Group by the id column
 *
 * @method Contabc1Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method Contabc1Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method Contabc1Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Contabc1Query leftJoinContabc($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contabc relation
 * @method Contabc1Query rightJoinContabc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contabc relation
 * @method Contabc1Query innerJoinContabc($relationAlias = null) Adds a INNER JOIN clause to the query using the Contabc relation
 *
 * @method Contabc1Query leftJoinContabb($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contabb relation
 * @method Contabc1Query rightJoinContabb($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contabb relation
 * @method Contabc1Query innerJoinContabb($relationAlias = null) Adds a INNER JOIN clause to the query using the Contabb relation
 *
 * @method Contabc1 findOne(PropelPDO $con = null) Return the first Contabc1 matching the query
 * @method Contabc1 findOneOrCreate(PropelPDO $con = null) Return the first Contabc1 matching the query, or a new Contabc1 object populated from the query conditions when no match is found
 *
 * @method Contabc1 findOneByNumcom(string $numcom) Return the first Contabc1 filtered by the numcom column
 * @method Contabc1 findOneByFeccom(string $feccom) Return the first Contabc1 filtered by the feccom column
 * @method Contabc1 findOneByDebcre(string $debcre) Return the first Contabc1 filtered by the debcre column
 * @method Contabc1 findOneByCodcta(string $codcta) Return the first Contabc1 filtered by the codcta column
 * @method Contabc1 findOneByNumasi(string $numasi) Return the first Contabc1 filtered by the numasi column
 * @method Contabc1 findOneByRefasi(string $refasi) Return the first Contabc1 filtered by the refasi column
 * @method Contabc1 findOneByDesasi(string $desasi) Return the first Contabc1 filtered by the desasi column
 * @method Contabc1 findOneByMonasi(string $monasi) Return the first Contabc1 filtered by the monasi column
 * @method Contabc1 findOneByCodcencos(string $codcencos) Return the first Contabc1 filtered by the codcencos column
 *
 * @method array findByNumcom(string $numcom) Return Contabc1 objects filtered by the numcom column
 * @method array findByFeccom(string $feccom) Return Contabc1 objects filtered by the feccom column
 * @method array findByDebcre(string $debcre) Return Contabc1 objects filtered by the debcre column
 * @method array findByCodcta(string $codcta) Return Contabc1 objects filtered by the codcta column
 * @method array findByNumasi(string $numasi) Return Contabc1 objects filtered by the numasi column
 * @method array findByRefasi(string $refasi) Return Contabc1 objects filtered by the refasi column
 * @method array findByDesasi(string $desasi) Return Contabc1 objects filtered by the desasi column
 * @method array findByMonasi(string $monasi) Return Contabc1 objects filtered by the monasi column
 * @method array findByCodcencos(string $codcencos) Return Contabc1 objects filtered by the codcencos column
 * @method array findById(int $id) Return Contabc1 objects filtered by the id column
 *
 * @package    propel.generator.lib.model.contabilidad.om
 */
abstract class BaseContabc1Query extends ModelCriteria
{
    /**
     * Initializes internal state of BaseContabc1Query object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Contabc1', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new Contabc1Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   Contabc1Query|Criteria $criteria Optional Criteria to build the query from
     *
     * @return Contabc1Query
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof Contabc1Query) {
            return $criteria;
        }
        $query = new Contabc1Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Contabc1|Contabc1[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = Contabc1Peer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(Contabc1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Contabc1 A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Contabc1 A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "numcom", "feccom", "debcre", "codcta", "numasi", "refasi", "desasi", "monasi", "codcencos", "id" FROM "contabc1" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Contabc1();
            $obj->hydrate($row);
            Contabc1Peer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Contabc1|Contabc1[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Contabc1[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Contabc1Peer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Contabc1Peer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the numcom column
     *
     * Example usage:
     * <code>
     * $query->filterByNumcom('fooValue');   // WHERE numcom = 'fooValue'
     * $query->filterByNumcom('%fooValue%'); // WHERE numcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByNumcom($numcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numcom)) {
                $numcom = str_replace('*', '%', $numcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::NUMCOM, $numcom, $comparison);
    }

    /**
     * Filter the query on the feccom column
     *
     * Example usage:
     * <code>
     * $query->filterByFeccom('2011-03-14'); // WHERE feccom = '2011-03-14'
     * $query->filterByFeccom('now'); // WHERE feccom = '2011-03-14'
     * $query->filterByFeccom(array('max' => 'yesterday')); // WHERE feccom > '2011-03-13'
     * </code>
     *
     * @param     mixed $feccom The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByFeccom($feccom = null, $comparison = null)
    {
        if (is_array($feccom)) {
            $useMinMax = false;
            if (isset($feccom['min'])) {
                $this->addUsingAlias(Contabc1Peer::FECCOM, $feccom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($feccom['max'])) {
                $this->addUsingAlias(Contabc1Peer::FECCOM, $feccom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::FECCOM, $feccom, $comparison);
    }

    /**
     * Filter the query on the debcre column
     *
     * Example usage:
     * <code>
     * $query->filterByDebcre('fooValue');   // WHERE debcre = 'fooValue'
     * $query->filterByDebcre('%fooValue%'); // WHERE debcre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $debcre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByDebcre($debcre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($debcre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $debcre)) {
                $debcre = str_replace('*', '%', $debcre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::DEBCRE, $debcre, $comparison);
    }

    /**
     * Filter the query on the codcta column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcta('fooValue');   // WHERE codcta = 'fooValue'
     * $query->filterByCodcta('%fooValue%'); // WHERE codcta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByCodcta($codcta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcta)) {
                $codcta = str_replace('*', '%', $codcta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::CODCTA, $codcta, $comparison);
    }

    /**
     * Filter the query on the numasi column
     *
     * Example usage:
     * <code>
     * $query->filterByNumasi(1234); // WHERE numasi = 1234
     * $query->filterByNumasi(array(12, 34)); // WHERE numasi IN (12, 34)
     * $query->filterByNumasi(array('min' => 12)); // WHERE numasi >= 12
     * $query->filterByNumasi(array('max' => 12)); // WHERE numasi <= 12
     * </code>
     *
     * @param     mixed $numasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByNumasi($numasi = null, $comparison = null)
    {
        if (is_array($numasi)) {
            $useMinMax = false;
            if (isset($numasi['min'])) {
                $this->addUsingAlias(Contabc1Peer::NUMASI, $numasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numasi['max'])) {
                $this->addUsingAlias(Contabc1Peer::NUMASI, $numasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::NUMASI, $numasi, $comparison);
    }

    /**
     * Filter the query on the refasi column
     *
     * Example usage:
     * <code>
     * $query->filterByRefasi('fooValue');   // WHERE refasi = 'fooValue'
     * $query->filterByRefasi('%fooValue%'); // WHERE refasi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refasi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByRefasi($refasi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refasi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refasi)) {
                $refasi = str_replace('*', '%', $refasi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::REFASI, $refasi, $comparison);
    }

    /**
     * Filter the query on the desasi column
     *
     * Example usage:
     * <code>
     * $query->filterByDesasi('fooValue');   // WHERE desasi = 'fooValue'
     * $query->filterByDesasi('%fooValue%'); // WHERE desasi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desasi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByDesasi($desasi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desasi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desasi)) {
                $desasi = str_replace('*', '%', $desasi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::DESASI, $desasi, $comparison);
    }

    /**
     * Filter the query on the monasi column
     *
     * Example usage:
     * <code>
     * $query->filterByMonasi(1234); // WHERE monasi = 1234
     * $query->filterByMonasi(array(12, 34)); // WHERE monasi IN (12, 34)
     * $query->filterByMonasi(array('min' => 12)); // WHERE monasi >= 12
     * $query->filterByMonasi(array('max' => 12)); // WHERE monasi <= 12
     * </code>
     *
     * @param     mixed $monasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByMonasi($monasi = null, $comparison = null)
    {
        if (is_array($monasi)) {
            $useMinMax = false;
            if (isset($monasi['min'])) {
                $this->addUsingAlias(Contabc1Peer::MONASI, $monasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monasi['max'])) {
                $this->addUsingAlias(Contabc1Peer::MONASI, $monasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::MONASI, $monasi, $comparison);
    }

    /**
     * Filter the query on the codcencos column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcencos('fooValue');   // WHERE codcencos = 'fooValue'
     * $query->filterByCodcencos('%fooValue%'); // WHERE codcencos LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcencos The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterByCodcencos($codcencos = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcencos)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcencos)) {
                $codcencos = str_replace('*', '%', $codcencos);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::CODCENCOS, $codcencos, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Contabc1Peer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Contabc1Peer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Contabc1Peer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Contabc object
     *
     * @param   Contabc|PropelObjectCollection $contabc The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 Contabc1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByContabc($contabc, $comparison = null)
    {
        if ($contabc instanceof Contabc) {
            return $this
                ->addUsingAlias(Contabc1Peer::NUMCOM, $contabc->getNumcom(), $comparison);
        } elseif ($contabc instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Contabc1Peer::NUMCOM, $contabc->toKeyValue('PrimaryKey', 'Numcom'), $comparison);
        } else {
            throw new PropelException('filterByContabc() only accepts arguments of type Contabc or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contabc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function joinContabc($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contabc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contabc');
        }

        return $this;
    }

    /**
     * Use the Contabc relation Contabc object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ContabcQuery A secondary query class using the current class as primary query
     */
    public function useContabcQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContabc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contabc', 'ContabcQuery');
    }

    /**
     * Filter the query by a related Contabb object
     *
     * @param   Contabb|PropelObjectCollection $contabb The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 Contabc1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByContabb($contabb, $comparison = null)
    {
        if ($contabb instanceof Contabb) {
            return $this
                ->addUsingAlias(Contabc1Peer::CODCTA, $contabb->getCodcta(), $comparison);
        } elseif ($contabb instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Contabc1Peer::CODCTA, $contabb->toKeyValue('PrimaryKey', 'Codcta'), $comparison);
        } else {
            throw new PropelException('filterByContabb() only accepts arguments of type Contabb or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contabb relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function joinContabb($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contabb');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contabb');
        }

        return $this;
    }

    /**
     * Use the Contabb relation Contabb object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ContabbQuery A secondary query class using the current class as primary query
     */
    public function useContabbQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContabb($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contabb', 'ContabbQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Contabc1 $contabc1 Object to remove from the list of results
     *
     * @return Contabc1Query The current query, for fluid interface
     */
    public function prune($contabc1 = null)
    {
        if ($contabc1) {
            $this->addUsingAlias(Contabc1Peer::ID, $contabc1->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
