<?php


/**
 * Base class that represents a query for the 'cprelapa' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:39 2015
 *
 * @method CprelapaQuery orderByRefrel($order = Criteria::ASC) Order by the refrel column
 * @method CprelapaQuery orderByTiprel($order = Criteria::ASC) Order by the tiprel column
 * @method CprelapaQuery orderByFecrel($order = Criteria::ASC) Order by the fecrel column
 * @method CprelapaQuery orderByRefapa($order = Criteria::ASC) Order by the refapa column
 * @method CprelapaQuery orderByDesrel($order = Criteria::ASC) Order by the desrel column
 * @method CprelapaQuery orderByDesanu($order = Criteria::ASC) Order by the desanu column
 * @method CprelapaQuery orderByMonrel($order = Criteria::ASC) Order by the monrel column
 * @method CprelapaQuery orderBySalaju($order = Criteria::ASC) Order by the salaju column
 * @method CprelapaQuery orderByStarel($order = Criteria::ASC) Order by the starel column
 * @method CprelapaQuery orderByFecanu($order = Criteria::ASC) Order by the fecanu column
 * @method CprelapaQuery orderByCedrif($order = Criteria::ASC) Order by the cedrif column
 * @method CprelapaQuery orderByNumcom($order = Criteria::ASC) Order by the numcom column
 * @method CprelapaQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CprelapaQuery groupByRefrel() Group by the refrel column
 * @method CprelapaQuery groupByTiprel() Group by the tiprel column
 * @method CprelapaQuery groupByFecrel() Group by the fecrel column
 * @method CprelapaQuery groupByRefapa() Group by the refapa column
 * @method CprelapaQuery groupByDesrel() Group by the desrel column
 * @method CprelapaQuery groupByDesanu() Group by the desanu column
 * @method CprelapaQuery groupByMonrel() Group by the monrel column
 * @method CprelapaQuery groupBySalaju() Group by the salaju column
 * @method CprelapaQuery groupByStarel() Group by the starel column
 * @method CprelapaQuery groupByFecanu() Group by the fecanu column
 * @method CprelapaQuery groupByCedrif() Group by the cedrif column
 * @method CprelapaQuery groupByNumcom() Group by the numcom column
 * @method CprelapaQuery groupById() Group by the id column
 *
 * @method CprelapaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CprelapaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CprelapaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Cprelapa findOne(PropelPDO $con = null) Return the first Cprelapa matching the query
 * @method Cprelapa findOneOrCreate(PropelPDO $con = null) Return the first Cprelapa matching the query, or a new Cprelapa object populated from the query conditions when no match is found
 *
 * @method Cprelapa findOneByRefrel(string $refrel) Return the first Cprelapa filtered by the refrel column
 * @method Cprelapa findOneByTiprel(string $tiprel) Return the first Cprelapa filtered by the tiprel column
 * @method Cprelapa findOneByFecrel(string $fecrel) Return the first Cprelapa filtered by the fecrel column
 * @method Cprelapa findOneByRefapa(string $refapa) Return the first Cprelapa filtered by the refapa column
 * @method Cprelapa findOneByDesrel(string $desrel) Return the first Cprelapa filtered by the desrel column
 * @method Cprelapa findOneByDesanu(string $desanu) Return the first Cprelapa filtered by the desanu column
 * @method Cprelapa findOneByMonrel(string $monrel) Return the first Cprelapa filtered by the monrel column
 * @method Cprelapa findOneBySalaju(string $salaju) Return the first Cprelapa filtered by the salaju column
 * @method Cprelapa findOneByStarel(string $starel) Return the first Cprelapa filtered by the starel column
 * @method Cprelapa findOneByFecanu(string $fecanu) Return the first Cprelapa filtered by the fecanu column
 * @method Cprelapa findOneByCedrif(string $cedrif) Return the first Cprelapa filtered by the cedrif column
 * @method Cprelapa findOneByNumcom(string $numcom) Return the first Cprelapa filtered by the numcom column
 *
 * @method array findByRefrel(string $refrel) Return Cprelapa objects filtered by the refrel column
 * @method array findByTiprel(string $tiprel) Return Cprelapa objects filtered by the tiprel column
 * @method array findByFecrel(string $fecrel) Return Cprelapa objects filtered by the fecrel column
 * @method array findByRefapa(string $refapa) Return Cprelapa objects filtered by the refapa column
 * @method array findByDesrel(string $desrel) Return Cprelapa objects filtered by the desrel column
 * @method array findByDesanu(string $desanu) Return Cprelapa objects filtered by the desanu column
 * @method array findByMonrel(string $monrel) Return Cprelapa objects filtered by the monrel column
 * @method array findBySalaju(string $salaju) Return Cprelapa objects filtered by the salaju column
 * @method array findByStarel(string $starel) Return Cprelapa objects filtered by the starel column
 * @method array findByFecanu(string $fecanu) Return Cprelapa objects filtered by the fecanu column
 * @method array findByCedrif(string $cedrif) Return Cprelapa objects filtered by the cedrif column
 * @method array findByNumcom(string $numcom) Return Cprelapa objects filtered by the numcom column
 * @method array findById(int $id) Return Cprelapa objects filtered by the id column
 *
 * @package    propel.generator.lib.model.presupuesto.om
 */
abstract class BaseCprelapaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCprelapaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Cprelapa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CprelapaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CprelapaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CprelapaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CprelapaQuery) {
            return $criteria;
        }
        $query = new CprelapaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Cprelapa|Cprelapa[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CprelapaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CprelapaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cprelapa A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Cprelapa A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "refrel", "tiprel", "fecrel", "refapa", "desrel", "desanu", "monrel", "salaju", "starel", "fecanu", "cedrif", "numcom", "id" FROM "cprelapa" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Cprelapa();
            $obj->hydrate($row);
            CprelapaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Cprelapa|Cprelapa[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Cprelapa[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CprelapaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CprelapaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the refrel column
     *
     * Example usage:
     * <code>
     * $query->filterByRefrel('fooValue');   // WHERE refrel = 'fooValue'
     * $query->filterByRefrel('%fooValue%'); // WHERE refrel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refrel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByRefrel($refrel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refrel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refrel)) {
                $refrel = str_replace('*', '%', $refrel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::REFREL, $refrel, $comparison);
    }

    /**
     * Filter the query on the tiprel column
     *
     * Example usage:
     * <code>
     * $query->filterByTiprel('fooValue');   // WHERE tiprel = 'fooValue'
     * $query->filterByTiprel('%fooValue%'); // WHERE tiprel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tiprel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByTiprel($tiprel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tiprel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tiprel)) {
                $tiprel = str_replace('*', '%', $tiprel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::TIPREL, $tiprel, $comparison);
    }

    /**
     * Filter the query on the fecrel column
     *
     * Example usage:
     * <code>
     * $query->filterByFecrel('2011-03-14'); // WHERE fecrel = '2011-03-14'
     * $query->filterByFecrel('now'); // WHERE fecrel = '2011-03-14'
     * $query->filterByFecrel(array('max' => 'yesterday')); // WHERE fecrel > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecrel The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByFecrel($fecrel = null, $comparison = null)
    {
        if (is_array($fecrel)) {
            $useMinMax = false;
            if (isset($fecrel['min'])) {
                $this->addUsingAlias(CprelapaPeer::FECREL, $fecrel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecrel['max'])) {
                $this->addUsingAlias(CprelapaPeer::FECREL, $fecrel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::FECREL, $fecrel, $comparison);
    }

    /**
     * Filter the query on the refapa column
     *
     * Example usage:
     * <code>
     * $query->filterByRefapa('fooValue');   // WHERE refapa = 'fooValue'
     * $query->filterByRefapa('%fooValue%'); // WHERE refapa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refapa The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByRefapa($refapa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refapa)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refapa)) {
                $refapa = str_replace('*', '%', $refapa);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::REFAPA, $refapa, $comparison);
    }

    /**
     * Filter the query on the desrel column
     *
     * Example usage:
     * <code>
     * $query->filterByDesrel('fooValue');   // WHERE desrel = 'fooValue'
     * $query->filterByDesrel('%fooValue%'); // WHERE desrel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desrel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByDesrel($desrel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desrel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desrel)) {
                $desrel = str_replace('*', '%', $desrel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::DESREL, $desrel, $comparison);
    }

    /**
     * Filter the query on the desanu column
     *
     * Example usage:
     * <code>
     * $query->filterByDesanu('fooValue');   // WHERE desanu = 'fooValue'
     * $query->filterByDesanu('%fooValue%'); // WHERE desanu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desanu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByDesanu($desanu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desanu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desanu)) {
                $desanu = str_replace('*', '%', $desanu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::DESANU, $desanu, $comparison);
    }

    /**
     * Filter the query on the monrel column
     *
     * Example usage:
     * <code>
     * $query->filterByMonrel(1234); // WHERE monrel = 1234
     * $query->filterByMonrel(array(12, 34)); // WHERE monrel IN (12, 34)
     * $query->filterByMonrel(array('min' => 12)); // WHERE monrel >= 12
     * $query->filterByMonrel(array('max' => 12)); // WHERE monrel <= 12
     * </code>
     *
     * @param     mixed $monrel The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByMonrel($monrel = null, $comparison = null)
    {
        if (is_array($monrel)) {
            $useMinMax = false;
            if (isset($monrel['min'])) {
                $this->addUsingAlias(CprelapaPeer::MONREL, $monrel['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($monrel['max'])) {
                $this->addUsingAlias(CprelapaPeer::MONREL, $monrel['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::MONREL, $monrel, $comparison);
    }

    /**
     * Filter the query on the salaju column
     *
     * Example usage:
     * <code>
     * $query->filterBySalaju(1234); // WHERE salaju = 1234
     * $query->filterBySalaju(array(12, 34)); // WHERE salaju IN (12, 34)
     * $query->filterBySalaju(array('min' => 12)); // WHERE salaju >= 12
     * $query->filterBySalaju(array('max' => 12)); // WHERE salaju <= 12
     * </code>
     *
     * @param     mixed $salaju The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterBySalaju($salaju = null, $comparison = null)
    {
        if (is_array($salaju)) {
            $useMinMax = false;
            if (isset($salaju['min'])) {
                $this->addUsingAlias(CprelapaPeer::SALAJU, $salaju['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salaju['max'])) {
                $this->addUsingAlias(CprelapaPeer::SALAJU, $salaju['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::SALAJU, $salaju, $comparison);
    }

    /**
     * Filter the query on the starel column
     *
     * Example usage:
     * <code>
     * $query->filterByStarel('fooValue');   // WHERE starel = 'fooValue'
     * $query->filterByStarel('%fooValue%'); // WHERE starel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $starel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByStarel($starel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($starel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $starel)) {
                $starel = str_replace('*', '%', $starel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::STAREL, $starel, $comparison);
    }

    /**
     * Filter the query on the fecanu column
     *
     * Example usage:
     * <code>
     * $query->filterByFecanu('2011-03-14'); // WHERE fecanu = '2011-03-14'
     * $query->filterByFecanu('now'); // WHERE fecanu = '2011-03-14'
     * $query->filterByFecanu(array('max' => 'yesterday')); // WHERE fecanu > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecanu The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByFecanu($fecanu = null, $comparison = null)
    {
        if (is_array($fecanu)) {
            $useMinMax = false;
            if (isset($fecanu['min'])) {
                $this->addUsingAlias(CprelapaPeer::FECANU, $fecanu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecanu['max'])) {
                $this->addUsingAlias(CprelapaPeer::FECANU, $fecanu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::FECANU, $fecanu, $comparison);
    }

    /**
     * Filter the query on the cedrif column
     *
     * Example usage:
     * <code>
     * $query->filterByCedrif('fooValue');   // WHERE cedrif = 'fooValue'
     * $query->filterByCedrif('%fooValue%'); // WHERE cedrif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cedrif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByCedrif($cedrif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cedrif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cedrif)) {
                $cedrif = str_replace('*', '%', $cedrif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::CEDRIF, $cedrif, $comparison);
    }

    /**
     * Filter the query on the numcom column
     *
     * Example usage:
     * <code>
     * $query->filterByNumcom('fooValue');   // WHERE numcom = 'fooValue'
     * $query->filterByNumcom('%fooValue%'); // WHERE numcom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numcom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterByNumcom($numcom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numcom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numcom)) {
                $numcom = str_replace('*', '%', $numcom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::NUMCOM, $numcom, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CprelapaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CprelapaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CprelapaPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Cprelapa $cprelapa Object to remove from the list of results
     *
     * @return CprelapaQuery The current query, for fluid interface
     */
    public function prune($cprelapa = null)
    {
        if ($cprelapa) {
            $this->addUsingAlias(CprelapaPeer::ID, $cprelapa->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
