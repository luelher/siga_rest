<?php



/**
 * This class defines the structure of the 'cadefcla' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:46 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.compras.map
 */
class CadefclaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.compras.map.CadefclaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cadefcla');
        $this->setPhpName('Cadefcla');
        $this->setClassname('Cadefcla');
        $this->setPackage('lib.model.compras');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('codcla', 'Codcla', 'VARCHAR', true, 4, null);
        $this->addColumn('descla', 'Descla', 'VARCHAR', false, 2500, null);
        $this->addColumn('tipcla', 'Tipcla', 'VARCHAR', false, 1, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CadefclaTableMap
