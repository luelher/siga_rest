<?php



/**
 * This class defines the structure of the 'carecaud' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:44 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.compras.map
 */
class CarecaudTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.compras.map.CarecaudTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('carecaud');
        $this->setPhpName('Carecaud');
        $this->setClassname('Carecaud');
        $this->setPackage('lib.model.compras');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('codrec', 'Codrec', 'VARCHAR', true, 10, null);
        $this->addColumn('desrec', 'Desrec', 'VARCHAR', true, 100, null);
        $this->addColumn('limrec', 'Limrec', 'VARCHAR', false, 1, null);
        $this->addColumn('fecemi', 'Fecemi', 'DATE', false, null, null);
        $this->addColumn('fecven', 'Fecven', 'DATE', false, null, null);
        $this->addColumn('canutr', 'Canutr', 'NUMERIC', false, 14, null);
        $this->addForeignKey('codtiprec', 'Codtiprec', 'VARCHAR', 'catiprec', 'codtiprec', true, 4, null);
        $this->addColumn('observ', 'Observ', 'VARCHAR', false, 100, null);
        $this->addColumn('puntua', 'Puntua', 'NUMERIC', false, 14, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Catiprec', 'Catiprec', RelationMap::MANY_TO_ONE, array('codtiprec' => 'codtiprec', ), null, null);
        $this->addRelation('Carecpro', 'Carecpro', RelationMap::ONE_TO_MANY, array('codrec' => 'codrec', ), null, null, 'Carecpros');
        $this->addRelation('Farecpro', 'Farecpro', RelationMap::ONE_TO_MANY, array('codrec' => 'codrec', ), null, null, 'Farecpros');
    } // buildRelations()

} // CarecaudTableMap
