<?php


/**
 * Base class that represents a query for the 'catalogo' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:44 2015
 *
 * @method CatalogoQuery orderByCodcta($order = Criteria::ASC) Order by the codcta column
 * @method CatalogoQuery orderByDescta($order = Criteria::ASC) Order by the descta column
 * @method CatalogoQuery orderByFecini($order = Criteria::ASC) Order by the fecini column
 * @method CatalogoQuery orderByFeccie($order = Criteria::ASC) Order by the feccie column
 * @method CatalogoQuery orderBySalant($order = Criteria::ASC) Order by the salant column
 * @method CatalogoQuery orderByDebcre($order = Criteria::ASC) Order by the debcre column
 * @method CatalogoQuery orderByCargab($order = Criteria::ASC) Order by the cargab column
 * @method CatalogoQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method CatalogoQuery groupByCodcta() Group by the codcta column
 * @method CatalogoQuery groupByDescta() Group by the descta column
 * @method CatalogoQuery groupByFecini() Group by the fecini column
 * @method CatalogoQuery groupByFeccie() Group by the feccie column
 * @method CatalogoQuery groupBySalant() Group by the salant column
 * @method CatalogoQuery groupByDebcre() Group by the debcre column
 * @method CatalogoQuery groupByCargab() Group by the cargab column
 * @method CatalogoQuery groupById() Group by the id column
 *
 * @method CatalogoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CatalogoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CatalogoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Catalogo findOne(PropelPDO $con = null) Return the first Catalogo matching the query
 * @method Catalogo findOneOrCreate(PropelPDO $con = null) Return the first Catalogo matching the query, or a new Catalogo object populated from the query conditions when no match is found
 *
 * @method Catalogo findOneByCodcta(string $codcta) Return the first Catalogo filtered by the codcta column
 * @method Catalogo findOneByDescta(string $descta) Return the first Catalogo filtered by the descta column
 * @method Catalogo findOneByFecini(string $fecini) Return the first Catalogo filtered by the fecini column
 * @method Catalogo findOneByFeccie(string $feccie) Return the first Catalogo filtered by the feccie column
 * @method Catalogo findOneBySalant(string $salant) Return the first Catalogo filtered by the salant column
 * @method Catalogo findOneByDebcre(string $debcre) Return the first Catalogo filtered by the debcre column
 * @method Catalogo findOneByCargab(string $cargab) Return the first Catalogo filtered by the cargab column
 *
 * @method array findByCodcta(string $codcta) Return Catalogo objects filtered by the codcta column
 * @method array findByDescta(string $descta) Return Catalogo objects filtered by the descta column
 * @method array findByFecini(string $fecini) Return Catalogo objects filtered by the fecini column
 * @method array findByFeccie(string $feccie) Return Catalogo objects filtered by the feccie column
 * @method array findBySalant(string $salant) Return Catalogo objects filtered by the salant column
 * @method array findByDebcre(string $debcre) Return Catalogo objects filtered by the debcre column
 * @method array findByCargab(string $cargab) Return Catalogo objects filtered by the cargab column
 * @method array findById(int $id) Return Catalogo objects filtered by the id column
 *
 * @package    propel.generator.lib.model.compras.om
 */
abstract class BaseCatalogoQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCatalogoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Catalogo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CatalogoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CatalogoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CatalogoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CatalogoQuery) {
            return $criteria;
        }
        $query = new CatalogoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Catalogo|Catalogo[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CatalogoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CatalogoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Catalogo A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Catalogo A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "codcta", "descta", "fecini", "feccie", "salant", "debcre", "cargab", "id" FROM "catalogo" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Catalogo();
            $obj->hydrate($row);
            CatalogoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Catalogo|Catalogo[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Catalogo[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CatalogoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CatalogoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the codcta column
     *
     * Example usage:
     * <code>
     * $query->filterByCodcta('fooValue');   // WHERE codcta = 'fooValue'
     * $query->filterByCodcta('%fooValue%'); // WHERE codcta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codcta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByCodcta($codcta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codcta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codcta)) {
                $codcta = str_replace('*', '%', $codcta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::CODCTA, $codcta, $comparison);
    }

    /**
     * Filter the query on the descta column
     *
     * Example usage:
     * <code>
     * $query->filterByDescta('fooValue');   // WHERE descta = 'fooValue'
     * $query->filterByDescta('%fooValue%'); // WHERE descta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descta The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByDescta($descta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descta)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descta)) {
                $descta = str_replace('*', '%', $descta);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::DESCTA, $descta, $comparison);
    }

    /**
     * Filter the query on the fecini column
     *
     * Example usage:
     * <code>
     * $query->filterByFecini('2011-03-14'); // WHERE fecini = '2011-03-14'
     * $query->filterByFecini('now'); // WHERE fecini = '2011-03-14'
     * $query->filterByFecini(array('max' => 'yesterday')); // WHERE fecini > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecini The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByFecini($fecini = null, $comparison = null)
    {
        if (is_array($fecini)) {
            $useMinMax = false;
            if (isset($fecini['min'])) {
                $this->addUsingAlias(CatalogoPeer::FECINI, $fecini['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecini['max'])) {
                $this->addUsingAlias(CatalogoPeer::FECINI, $fecini['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::FECINI, $fecini, $comparison);
    }

    /**
     * Filter the query on the feccie column
     *
     * Example usage:
     * <code>
     * $query->filterByFeccie('2011-03-14'); // WHERE feccie = '2011-03-14'
     * $query->filterByFeccie('now'); // WHERE feccie = '2011-03-14'
     * $query->filterByFeccie(array('max' => 'yesterday')); // WHERE feccie > '2011-03-13'
     * </code>
     *
     * @param     mixed $feccie The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByFeccie($feccie = null, $comparison = null)
    {
        if (is_array($feccie)) {
            $useMinMax = false;
            if (isset($feccie['min'])) {
                $this->addUsingAlias(CatalogoPeer::FECCIE, $feccie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($feccie['max'])) {
                $this->addUsingAlias(CatalogoPeer::FECCIE, $feccie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::FECCIE, $feccie, $comparison);
    }

    /**
     * Filter the query on the salant column
     *
     * Example usage:
     * <code>
     * $query->filterBySalant(1234); // WHERE salant = 1234
     * $query->filterBySalant(array(12, 34)); // WHERE salant IN (12, 34)
     * $query->filterBySalant(array('min' => 12)); // WHERE salant >= 12
     * $query->filterBySalant(array('max' => 12)); // WHERE salant <= 12
     * </code>
     *
     * @param     mixed $salant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterBySalant($salant = null, $comparison = null)
    {
        if (is_array($salant)) {
            $useMinMax = false;
            if (isset($salant['min'])) {
                $this->addUsingAlias(CatalogoPeer::SALANT, $salant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salant['max'])) {
                $this->addUsingAlias(CatalogoPeer::SALANT, $salant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::SALANT, $salant, $comparison);
    }

    /**
     * Filter the query on the debcre column
     *
     * Example usage:
     * <code>
     * $query->filterByDebcre('fooValue');   // WHERE debcre = 'fooValue'
     * $query->filterByDebcre('%fooValue%'); // WHERE debcre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $debcre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByDebcre($debcre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($debcre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $debcre)) {
                $debcre = str_replace('*', '%', $debcre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::DEBCRE, $debcre, $comparison);
    }

    /**
     * Filter the query on the cargab column
     *
     * Example usage:
     * <code>
     * $query->filterByCargab('fooValue');   // WHERE cargab = 'fooValue'
     * $query->filterByCargab('%fooValue%'); // WHERE cargab LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cargab The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterByCargab($cargab = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cargab)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cargab)) {
                $cargab = str_replace('*', '%', $cargab);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::CARGAB, $cargab, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CatalogoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CatalogoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatalogoPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Catalogo $catalogo Object to remove from the list of results
     *
     * @return CatalogoQuery The current query, for fluid interface
     */
    public function prune($catalogo = null)
    {
        if ($catalogo) {
            $this->addUsingAlias(CatalogoPeer::ID, $catalogo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
