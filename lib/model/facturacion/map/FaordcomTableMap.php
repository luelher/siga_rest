<?php



/**
 * This class defines the structure of the 'faordcom' table.
 *
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:50 2015
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.lib.model.facturacion.map
 */
class FaordcomTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'lib.model.facturacion.map.FaordcomTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('faordcom');
        $this->setPhpName('Faordcom');
        $this->setClassname('Faordcom');
        $this->setPackage('lib.model.facturacion');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('ordcom', 'Ordcom', 'VARCHAR', true, 8, null);
        $this->addColumn('fecord', 'Fecord', 'TIMESTAMP', false, null, null);
        $this->addColumn('desord', 'Desord', 'VARCHAR', true, 1000, null);
        $this->addColumn('monord', 'Monord', 'NUMERIC', false, 14, null);
        $this->addColumn('cedrif', 'Cedrif', 'VARCHAR', true, 20, null);
        $this->addColumn('nompro', 'Nompro', 'VARCHAR', true, 200, null);
        $this->addColumn('dirpro', 'Dirpro', 'VARCHAR', true, 1000, null);
        $this->addColumn('codalmsap', 'Codalmsap', 'VARCHAR', true, 50, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // FaordcomTableMap
