<?php

/**
 * Subclass for representing a row from the 'fadetpre'.
 *
 *
 *
 * @package    Roraima
 * @subpackage lib.model
 * @author     $Author: dmartinez $ <desarrollo@cidesa.com.ve>
 * @version SVN: $Id: Fadetpre.php 55777 2014-03-06 20:01:57Z dmartinez $
 *
 * @copyright  Copyright 2007, Cide S.A.
 * @license    http://opensource.org/licenses/gpl-2.0.php GPLv2
 */
class Fadetpre extends BaseFadetpre
{
	protected $obj = array();
    protected $desart="";
	protected $canord="0,00";
	protected $candes="0,00";
	protected $canaju="0,00";
	protected $cantot="0,00";
	protected $preart="0,00";
	protected $precioe="0,00";
	protected $totart2="0,00";
	protected $porrgo="0,00";
  protected $btucon="0,00";
  protected $check="";
  protected $recargos="";
  protected $contratos="";
  protected $tipocon="";


  public function getDesart()
  {
   return Herramientas::getX('CODART','Caregart','Desart',self::getCodart());
  }

   public function afterHydrate()
  {
    if (self::getPrecio()!=0)
    {
      $this->precioe=number_format(self::getPrecio(), 2, ',', '.');
    }
    $porcrgo=0;
    $c= new Criteria();
    $c->add(FarecargPeer::TIPRGO,'P');
    $this->sql = "codrgo in (select codrgo from farecart where codart = '".self::getCodart()."')";
	$c->add(FarecargPeer::CODRGO,$this->sql,Criteria :: CUSTOM);
    $reg=FarecargPeer::doSelect($c);
	if ($reg){
	 foreach ($reg as $sum)
	 {
	   $porcrgo += $sum->getMonrgo();
	 }
	}
    $this->porrgo=number_format($porcrgo,2,',','.');

    $this->canord=number_format(self::getCansol(), 2, ',', '.');
    $this->preart=number_format(self::getPrecio(), 2, ',', '.');
    $this->cantot=number_format(self::getCansol(), 2, ',', '.');
    $val=self::getPrecio() * self::getCansol() + self::getMonrgo();
    $this->totart2=number_format($val, 2, ',', '.');

    if (self::getMonrgo()>0 || self::getMondesc()>0)
     $this->check='1';

    $a= new Criteria();
    $a->add(FargoprePeer::REFDOC,self::getRefpre());
    $a->add(FargoprePeer::CODART,self::getCodart());
    $result=FargoprePeer::doSelect($a);
    if ($result)
    {   $this->recargos="";
        foreach ($result as $datos)
        {
           $monrgoq=H::FormatoMonto($datos->getMonrgo());
           $monrgoc=H::FormatoMonto($datos->getMonrgo2());
           $this->recargos=$this->recargos.$datos->getCodrgo().'_'.$datos->getNomrgo().'_'.$datos->getRecfij().'_' .$monrgoq.'_'.$datos->getCodcta().'_'.$datos->getTipo().'_'.$monrgoc.'!';
        }
    }

    $q= new Criteria();
    $q->add(FadetconPeer::REFPRE,self::getRefpre());
    $q->add(FadetconPeer::CODART,self::getCodart());
  //  $q->addJoin(FadetconPeer::PERCON,FapresupPeer::PERCON);
    $q->addJoin(FadetconPeer::REFPRE,FapresupPeer::REFPRE);
    $resultq=FadetconPeer::doSelect($q);
    if ($resultq)
    {
        foreach ($resultq as $datosq)
        {
           $this->contratos=$this->contratos.date('d/m/Y',strtotime($datosq->getFecini())).'_'.date('d/m/Y', strtotime($datosq->getFecfin())).'_'.$datosq->getCancon().'_'.self::getCansol().'!';
        }
    }

    if (self::getId()){
      $this->tipocon=H::getX_vacio('REFPRE','Fapresup','Percon',self::getRefpre());
    }
  }


}
