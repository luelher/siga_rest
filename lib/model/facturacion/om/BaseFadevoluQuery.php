<?php


/**
 * Base class that represents a query for the 'fadevolu' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:47 2015
 *
 * @method FadevoluQuery orderByNrodev($order = Criteria::ASC) Order by the nrodev column
 * @method FadevoluQuery orderByFecdev($order = Criteria::ASC) Order by the fecdev column
 * @method FadevoluQuery orderByRefdes($order = Criteria::ASC) Order by the refdes column
 * @method FadevoluQuery orderByFatipdevId($order = Criteria::ASC) Order by the fatipdev_id column
 * @method FadevoluQuery orderByCodalm($order = Criteria::ASC) Order by the codalm column
 * @method FadevoluQuery orderByDesdev($order = Criteria::ASC) Order by the desdev column
 * @method FadevoluQuery orderByStadph($order = Criteria::ASC) Order by the stadph column
 * @method FadevoluQuery orderByMondev($order = Criteria::ASC) Order by the mondev column
 * @method FadevoluQuery orderByObsdev($order = Criteria::ASC) Order by the obsdev column
 * @method FadevoluQuery orderByTipdev($order = Criteria::ASC) Order by the tipdev column
 * @method FadevoluQuery orderByCoddirec($order = Criteria::ASC) Order by the coddirec column
 * @method FadevoluQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FadevoluQuery groupByNrodev() Group by the nrodev column
 * @method FadevoluQuery groupByFecdev() Group by the fecdev column
 * @method FadevoluQuery groupByRefdes() Group by the refdes column
 * @method FadevoluQuery groupByFatipdevId() Group by the fatipdev_id column
 * @method FadevoluQuery groupByCodalm() Group by the codalm column
 * @method FadevoluQuery groupByDesdev() Group by the desdev column
 * @method FadevoluQuery groupByStadph() Group by the stadph column
 * @method FadevoluQuery groupByMondev() Group by the mondev column
 * @method FadevoluQuery groupByObsdev() Group by the obsdev column
 * @method FadevoluQuery groupByTipdev() Group by the tipdev column
 * @method FadevoluQuery groupByCoddirec() Group by the coddirec column
 * @method FadevoluQuery groupById() Group by the id column
 *
 * @method FadevoluQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FadevoluQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FadevoluQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FadevoluQuery leftJoinFatipdev($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fatipdev relation
 * @method FadevoluQuery rightJoinFatipdev($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fatipdev relation
 * @method FadevoluQuery innerJoinFatipdev($relationAlias = null) Adds a INNER JOIN clause to the query using the Fatipdev relation
 *
 * @method Fadevolu findOne(PropelPDO $con = null) Return the first Fadevolu matching the query
 * @method Fadevolu findOneOrCreate(PropelPDO $con = null) Return the first Fadevolu matching the query, or a new Fadevolu object populated from the query conditions when no match is found
 *
 * @method Fadevolu findOneByNrodev(string $nrodev) Return the first Fadevolu filtered by the nrodev column
 * @method Fadevolu findOneByFecdev(string $fecdev) Return the first Fadevolu filtered by the fecdev column
 * @method Fadevolu findOneByRefdes(string $refdes) Return the first Fadevolu filtered by the refdes column
 * @method Fadevolu findOneByFatipdevId(int $fatipdev_id) Return the first Fadevolu filtered by the fatipdev_id column
 * @method Fadevolu findOneByCodalm(string $codalm) Return the first Fadevolu filtered by the codalm column
 * @method Fadevolu findOneByDesdev(string $desdev) Return the first Fadevolu filtered by the desdev column
 * @method Fadevolu findOneByStadph(string $stadph) Return the first Fadevolu filtered by the stadph column
 * @method Fadevolu findOneByMondev(string $mondev) Return the first Fadevolu filtered by the mondev column
 * @method Fadevolu findOneByObsdev(string $obsdev) Return the first Fadevolu filtered by the obsdev column
 * @method Fadevolu findOneByTipdev(string $tipdev) Return the first Fadevolu filtered by the tipdev column
 * @method Fadevolu findOneByCoddirec(string $coddirec) Return the first Fadevolu filtered by the coddirec column
 *
 * @method array findByNrodev(string $nrodev) Return Fadevolu objects filtered by the nrodev column
 * @method array findByFecdev(string $fecdev) Return Fadevolu objects filtered by the fecdev column
 * @method array findByRefdes(string $refdes) Return Fadevolu objects filtered by the refdes column
 * @method array findByFatipdevId(int $fatipdev_id) Return Fadevolu objects filtered by the fatipdev_id column
 * @method array findByCodalm(string $codalm) Return Fadevolu objects filtered by the codalm column
 * @method array findByDesdev(string $desdev) Return Fadevolu objects filtered by the desdev column
 * @method array findByStadph(string $stadph) Return Fadevolu objects filtered by the stadph column
 * @method array findByMondev(string $mondev) Return Fadevolu objects filtered by the mondev column
 * @method array findByObsdev(string $obsdev) Return Fadevolu objects filtered by the obsdev column
 * @method array findByTipdev(string $tipdev) Return Fadevolu objects filtered by the tipdev column
 * @method array findByCoddirec(string $coddirec) Return Fadevolu objects filtered by the coddirec column
 * @method array findById(int $id) Return Fadevolu objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFadevoluQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFadevoluQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Fadevolu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FadevoluQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FadevoluQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FadevoluQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FadevoluQuery) {
            return $criteria;
        }
        $query = new FadevoluQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Fadevolu|Fadevolu[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FadevoluPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FadevoluPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fadevolu A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fadevolu A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "nrodev", "fecdev", "refdes", "fatipdev_id", "codalm", "desdev", "stadph", "mondev", "obsdev", "tipdev", "coddirec", "id" FROM "fadevolu" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Fadevolu();
            $obj->hydrate($row);
            FadevoluPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Fadevolu|Fadevolu[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Fadevolu[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FadevoluPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FadevoluPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the nrodev column
     *
     * Example usage:
     * <code>
     * $query->filterByNrodev('fooValue');   // WHERE nrodev = 'fooValue'
     * $query->filterByNrodev('%fooValue%'); // WHERE nrodev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nrodev The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByNrodev($nrodev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nrodev)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nrodev)) {
                $nrodev = str_replace('*', '%', $nrodev);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::NRODEV, $nrodev, $comparison);
    }

    /**
     * Filter the query on the fecdev column
     *
     * Example usage:
     * <code>
     * $query->filterByFecdev('2011-03-14'); // WHERE fecdev = '2011-03-14'
     * $query->filterByFecdev('now'); // WHERE fecdev = '2011-03-14'
     * $query->filterByFecdev(array('max' => 'yesterday')); // WHERE fecdev > '2011-03-13'
     * </code>
     *
     * @param     mixed $fecdev The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByFecdev($fecdev = null, $comparison = null)
    {
        if (is_array($fecdev)) {
            $useMinMax = false;
            if (isset($fecdev['min'])) {
                $this->addUsingAlias(FadevoluPeer::FECDEV, $fecdev['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fecdev['max'])) {
                $this->addUsingAlias(FadevoluPeer::FECDEV, $fecdev['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::FECDEV, $fecdev, $comparison);
    }

    /**
     * Filter the query on the refdes column
     *
     * Example usage:
     * <code>
     * $query->filterByRefdes('fooValue');   // WHERE refdes = 'fooValue'
     * $query->filterByRefdes('%fooValue%'); // WHERE refdes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refdes The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByRefdes($refdes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refdes)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refdes)) {
                $refdes = str_replace('*', '%', $refdes);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::REFDES, $refdes, $comparison);
    }

    /**
     * Filter the query on the fatipdev_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFatipdevId(1234); // WHERE fatipdev_id = 1234
     * $query->filterByFatipdevId(array(12, 34)); // WHERE fatipdev_id IN (12, 34)
     * $query->filterByFatipdevId(array('min' => 12)); // WHERE fatipdev_id >= 12
     * $query->filterByFatipdevId(array('max' => 12)); // WHERE fatipdev_id <= 12
     * </code>
     *
     * @see       filterByFatipdev()
     *
     * @param     mixed $fatipdevId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByFatipdevId($fatipdevId = null, $comparison = null)
    {
        if (is_array($fatipdevId)) {
            $useMinMax = false;
            if (isset($fatipdevId['min'])) {
                $this->addUsingAlias(FadevoluPeer::FATIPDEV_ID, $fatipdevId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fatipdevId['max'])) {
                $this->addUsingAlias(FadevoluPeer::FATIPDEV_ID, $fatipdevId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::FATIPDEV_ID, $fatipdevId, $comparison);
    }

    /**
     * Filter the query on the codalm column
     *
     * Example usage:
     * <code>
     * $query->filterByCodalm('fooValue');   // WHERE codalm = 'fooValue'
     * $query->filterByCodalm('%fooValue%'); // WHERE codalm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codalm The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByCodalm($codalm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codalm)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codalm)) {
                $codalm = str_replace('*', '%', $codalm);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::CODALM, $codalm, $comparison);
    }

    /**
     * Filter the query on the desdev column
     *
     * Example usage:
     * <code>
     * $query->filterByDesdev('fooValue');   // WHERE desdev = 'fooValue'
     * $query->filterByDesdev('%fooValue%'); // WHERE desdev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desdev The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByDesdev($desdev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desdev)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desdev)) {
                $desdev = str_replace('*', '%', $desdev);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::DESDEV, $desdev, $comparison);
    }

    /**
     * Filter the query on the stadph column
     *
     * Example usage:
     * <code>
     * $query->filterByStadph('fooValue');   // WHERE stadph = 'fooValue'
     * $query->filterByStadph('%fooValue%'); // WHERE stadph LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stadph The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByStadph($stadph = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stadph)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $stadph)) {
                $stadph = str_replace('*', '%', $stadph);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::STADPH, $stadph, $comparison);
    }

    /**
     * Filter the query on the mondev column
     *
     * Example usage:
     * <code>
     * $query->filterByMondev(1234); // WHERE mondev = 1234
     * $query->filterByMondev(array(12, 34)); // WHERE mondev IN (12, 34)
     * $query->filterByMondev(array('min' => 12)); // WHERE mondev >= 12
     * $query->filterByMondev(array('max' => 12)); // WHERE mondev <= 12
     * </code>
     *
     * @param     mixed $mondev The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByMondev($mondev = null, $comparison = null)
    {
        if (is_array($mondev)) {
            $useMinMax = false;
            if (isset($mondev['min'])) {
                $this->addUsingAlias(FadevoluPeer::MONDEV, $mondev['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mondev['max'])) {
                $this->addUsingAlias(FadevoluPeer::MONDEV, $mondev['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::MONDEV, $mondev, $comparison);
    }

    /**
     * Filter the query on the obsdev column
     *
     * Example usage:
     * <code>
     * $query->filterByObsdev('fooValue');   // WHERE obsdev = 'fooValue'
     * $query->filterByObsdev('%fooValue%'); // WHERE obsdev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obsdev The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByObsdev($obsdev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obsdev)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $obsdev)) {
                $obsdev = str_replace('*', '%', $obsdev);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::OBSDEV, $obsdev, $comparison);
    }

    /**
     * Filter the query on the tipdev column
     *
     * Example usage:
     * <code>
     * $query->filterByTipdev('fooValue');   // WHERE tipdev = 'fooValue'
     * $query->filterByTipdev('%fooValue%'); // WHERE tipdev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipdev The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByTipdev($tipdev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipdev)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipdev)) {
                $tipdev = str_replace('*', '%', $tipdev);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::TIPDEV, $tipdev, $comparison);
    }

    /**
     * Filter the query on the coddirec column
     *
     * Example usage:
     * <code>
     * $query->filterByCoddirec('fooValue');   // WHERE coddirec = 'fooValue'
     * $query->filterByCoddirec('%fooValue%'); // WHERE coddirec LIKE '%fooValue%'
     * </code>
     *
     * @param     string $coddirec The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterByCoddirec($coddirec = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($coddirec)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $coddirec)) {
                $coddirec = str_replace('*', '%', $coddirec);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::CODDIREC, $coddirec, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FadevoluPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FadevoluPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadevoluPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query by a related Fatipdev object
     *
     * @param   Fatipdev|PropelObjectCollection $fatipdev The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FadevoluQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByFatipdev($fatipdev, $comparison = null)
    {
        if ($fatipdev instanceof Fatipdev) {
            return $this
                ->addUsingAlias(FadevoluPeer::FATIPDEV_ID, $fatipdev->getId(), $comparison);
        } elseif ($fatipdev instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FadevoluPeer::FATIPDEV_ID, $fatipdev->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFatipdev() only accepts arguments of type Fatipdev or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fatipdev relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function joinFatipdev($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fatipdev');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fatipdev');
        }

        return $this;
    }

    /**
     * Use the Fatipdev relation Fatipdev object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   FatipdevQuery A secondary query class using the current class as primary query
     */
    public function useFatipdevQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFatipdev($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fatipdev', 'FatipdevQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Fadevolu $fadevolu Object to remove from the list of results
     *
     * @return FadevoluQuery The current query, for fluid interface
     */
    public function prune($fadevolu = null)
    {
        if ($fadevolu) {
            $this->addUsingAlias(FadevoluPeer::ID, $fadevolu->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
