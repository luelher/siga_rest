<?php


/**
 * Base class that represents a query for the 'fadescartpro' table.
 *
 * null
 *
 * This class was autogenerated by Propel 1.6.9 on:
 *
 * Fri Mar 20 16:04:49 2015
 *
 * @method FadescartproQuery orderByCoddesc($order = Criteria::ASC) Order by the coddesc column
 * @method FadescartproQuery orderByRefdoc($order = Criteria::ASC) Order by the refdoc column
 * @method FadescartproQuery orderByCodart($order = Criteria::ASC) Order by the codart column
 * @method FadescartproQuery orderByMondesc($order = Criteria::ASC) Order by the mondesc column
 * @method FadescartproQuery orderByMondetdesc($order = Criteria::ASC) Order by the mondetdesc column
 * @method FadescartproQuery orderByTipdoc($order = Criteria::ASC) Order by the tipdoc column
 * @method FadescartproQuery orderByDesart($order = Criteria::ASC) Order by the desart column
 * @method FadescartproQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method FadescartproQuery groupByCoddesc() Group by the coddesc column
 * @method FadescartproQuery groupByRefdoc() Group by the refdoc column
 * @method FadescartproQuery groupByCodart() Group by the codart column
 * @method FadescartproQuery groupByMondesc() Group by the mondesc column
 * @method FadescartproQuery groupByMondetdesc() Group by the mondetdesc column
 * @method FadescartproQuery groupByTipdoc() Group by the tipdoc column
 * @method FadescartproQuery groupByDesart() Group by the desart column
 * @method FadescartproQuery groupById() Group by the id column
 *
 * @method FadescartproQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FadescartproQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FadescartproQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Fadescartpro findOne(PropelPDO $con = null) Return the first Fadescartpro matching the query
 * @method Fadescartpro findOneOrCreate(PropelPDO $con = null) Return the first Fadescartpro matching the query, or a new Fadescartpro object populated from the query conditions when no match is found
 *
 * @method Fadescartpro findOneByCoddesc(string $coddesc) Return the first Fadescartpro filtered by the coddesc column
 * @method Fadescartpro findOneByRefdoc(string $refdoc) Return the first Fadescartpro filtered by the refdoc column
 * @method Fadescartpro findOneByCodart(string $codart) Return the first Fadescartpro filtered by the codart column
 * @method Fadescartpro findOneByMondesc(string $mondesc) Return the first Fadescartpro filtered by the mondesc column
 * @method Fadescartpro findOneByMondetdesc(string $mondetdesc) Return the first Fadescartpro filtered by the mondetdesc column
 * @method Fadescartpro findOneByTipdoc(string $tipdoc) Return the first Fadescartpro filtered by the tipdoc column
 * @method Fadescartpro findOneByDesart(string $desart) Return the first Fadescartpro filtered by the desart column
 *
 * @method array findByCoddesc(string $coddesc) Return Fadescartpro objects filtered by the coddesc column
 * @method array findByRefdoc(string $refdoc) Return Fadescartpro objects filtered by the refdoc column
 * @method array findByCodart(string $codart) Return Fadescartpro objects filtered by the codart column
 * @method array findByMondesc(string $mondesc) Return Fadescartpro objects filtered by the mondesc column
 * @method array findByMondetdesc(string $mondetdesc) Return Fadescartpro objects filtered by the mondetdesc column
 * @method array findByTipdoc(string $tipdoc) Return Fadescartpro objects filtered by the tipdoc column
 * @method array findByDesart(string $desart) Return Fadescartpro objects filtered by the desart column
 * @method array findById(int $id) Return Fadescartpro objects filtered by the id column
 *
 * @package    propel.generator.lib.model.facturacion.om
 */
abstract class BaseFadescartproQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFadescartproQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'simaxxx', $modelName = 'Fadescartpro', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FadescartproQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FadescartproQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FadescartproQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FadescartproQuery) {
            return $criteria;
        }
        $query = new FadescartproQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Fadescartpro|Fadescartpro[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FadescartproPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FadescartproPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fadescartpro A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Fadescartpro A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT "coddesc", "refdoc", "codart", "mondesc", "mondetdesc", "tipdoc", "desart", "id" FROM "fadescartpro" WHERE "id" = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Fadescartpro();
            $obj->hydrate($row);
            FadescartproPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Fadescartpro|Fadescartpro[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Fadescartpro[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FadescartproPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FadescartproPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the coddesc column
     *
     * Example usage:
     * <code>
     * $query->filterByCoddesc('fooValue');   // WHERE coddesc = 'fooValue'
     * $query->filterByCoddesc('%fooValue%'); // WHERE coddesc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $coddesc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByCoddesc($coddesc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($coddesc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $coddesc)) {
                $coddesc = str_replace('*', '%', $coddesc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::CODDESC, $coddesc, $comparison);
    }

    /**
     * Filter the query on the refdoc column
     *
     * Example usage:
     * <code>
     * $query->filterByRefdoc('fooValue');   // WHERE refdoc = 'fooValue'
     * $query->filterByRefdoc('%fooValue%'); // WHERE refdoc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refdoc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByRefdoc($refdoc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refdoc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refdoc)) {
                $refdoc = str_replace('*', '%', $refdoc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::REFDOC, $refdoc, $comparison);
    }

    /**
     * Filter the query on the codart column
     *
     * Example usage:
     * <code>
     * $query->filterByCodart('fooValue');   // WHERE codart = 'fooValue'
     * $query->filterByCodart('%fooValue%'); // WHERE codart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByCodart($codart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codart)) {
                $codart = str_replace('*', '%', $codart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::CODART, $codart, $comparison);
    }

    /**
     * Filter the query on the mondesc column
     *
     * Example usage:
     * <code>
     * $query->filterByMondesc(1234); // WHERE mondesc = 1234
     * $query->filterByMondesc(array(12, 34)); // WHERE mondesc IN (12, 34)
     * $query->filterByMondesc(array('min' => 12)); // WHERE mondesc >= 12
     * $query->filterByMondesc(array('max' => 12)); // WHERE mondesc <= 12
     * </code>
     *
     * @param     mixed $mondesc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByMondesc($mondesc = null, $comparison = null)
    {
        if (is_array($mondesc)) {
            $useMinMax = false;
            if (isset($mondesc['min'])) {
                $this->addUsingAlias(FadescartproPeer::MONDESC, $mondesc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mondesc['max'])) {
                $this->addUsingAlias(FadescartproPeer::MONDESC, $mondesc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::MONDESC, $mondesc, $comparison);
    }

    /**
     * Filter the query on the mondetdesc column
     *
     * Example usage:
     * <code>
     * $query->filterByMondetdesc(1234); // WHERE mondetdesc = 1234
     * $query->filterByMondetdesc(array(12, 34)); // WHERE mondetdesc IN (12, 34)
     * $query->filterByMondetdesc(array('min' => 12)); // WHERE mondetdesc >= 12
     * $query->filterByMondetdesc(array('max' => 12)); // WHERE mondetdesc <= 12
     * </code>
     *
     * @param     mixed $mondetdesc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByMondetdesc($mondetdesc = null, $comparison = null)
    {
        if (is_array($mondetdesc)) {
            $useMinMax = false;
            if (isset($mondetdesc['min'])) {
                $this->addUsingAlias(FadescartproPeer::MONDETDESC, $mondetdesc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mondetdesc['max'])) {
                $this->addUsingAlias(FadescartproPeer::MONDETDESC, $mondetdesc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::MONDETDESC, $mondetdesc, $comparison);
    }

    /**
     * Filter the query on the tipdoc column
     *
     * Example usage:
     * <code>
     * $query->filterByTipdoc('fooValue');   // WHERE tipdoc = 'fooValue'
     * $query->filterByTipdoc('%fooValue%'); // WHERE tipdoc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipdoc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByTipdoc($tipdoc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipdoc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tipdoc)) {
                $tipdoc = str_replace('*', '%', $tipdoc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::TIPDOC, $tipdoc, $comparison);
    }

    /**
     * Filter the query on the desart column
     *
     * Example usage:
     * <code>
     * $query->filterByDesart('fooValue');   // WHERE desart = 'fooValue'
     * $query->filterByDesart('%fooValue%'); // WHERE desart LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desart The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterByDesart($desart = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desart)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desart)) {
                $desart = str_replace('*', '%', $desart);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::DESART, $desart, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FadescartproPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FadescartproPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FadescartproPeer::ID, $id, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Fadescartpro $fadescartpro Object to remove from the list of results
     *
     * @return FadescartproQuery The current query, for fluid interface
     */
    public function prune($fadescartpro = null)
    {
        if ($fadescartpro) {
            $this->addUsingAlias(FadescartproPeer::ID, $fadescartpro->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
