
-----------------------------------------------------------------------
-- caajuoc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caajuoc" CASCADE;

CREATE TABLE "caajuoc"
(
    "ajuoc" VARCHAR(8) NOT NULL,
    "ordcom" VARCHAR(8) NOT NULL,
    "fecaju" DATE,
    "desaju" VARCHAR(100),
    "monaju" NUMERIC(14,2),
    "staaju" VARCHAR(1),
    "refaju" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caajuoc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartalm" CASCADE;

CREATE TABLE "caartalm"
(
    "codalm" VARCHAR(20) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20),
    "eximin" NUMERIC(14,2),
    "eximax" NUMERIC(14,2),
    "exiact" NUMERIC(14,2),
    "ptoreo" NUMERIC(14,2),
    "pedmin" NUMERIC(14,2),
    "pedmax" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartalm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartaoc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartaoc" CASCADE;

CREATE TABLE "caartaoc"
(
    "ajuoc" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(32) NOT NULL,
    "canord" NUMERIC(14,2),
    "canaju" NUMERIC(14,2),
    "montot" NUMERIC(14,2),
    "monrgo" NUMERIC(14,2),
    "monaju" NUMERIC(14,2),
    "monrec" NUMERIC(14,2),
    "desart" VARCHAR(2000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartaoc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartdph
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartdph" CASCADE;

CREATE TABLE "caartdph"
(
    "dphart" VARCHAR(15) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(16) NOT NULL,
    "candph" NUMERIC(14,2),
    "candev" NUMERIC(14,2),
    "cantot" NUMERIC(14,2),
    "preart" NUMERIC(14,2),
    "montot" NUMERIC(14,2),
    "numlot" VARCHAR(100),
    "canent" NUMERIC(14,2),
    "codfal" VARCHAR(3),
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartdph"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartdphser
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartdphser" CASCADE;

CREATE TABLE "caartdphser"
(
    "dphart" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(16) NOT NULL,
    "fecrea" DATE NOT NULL,
    "nomemp" VARCHAR(100),
    "dphobs" VARCHAR(255),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartdphser"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartfec
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartfec" CASCADE;

CREATE TABLE "caartfec"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "desart" VARCHAR(1000),
    "canart" NUMERIC(14,2) NOT NULL,
    "fecent" DATE NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartfec"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartord
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartord" CASCADE;

CREATE TABLE "caartord"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(16) NOT NULL,
    "canord" NUMERIC(14,2),
    "canaju" NUMERIC(14,2),
    "canrec" NUMERIC(14,2),
    "cantot" NUMERIC(14,2),
    "preart" NUMERIC(14,6),
    "dtoart" NUMERIC(14,2),
    "codrgo" VARCHAR(32),
    "cerart" VARCHAR(1),
    "rgoart" NUMERIC(14,4),
    "totart" NUMERIC(14,6),
    "fecent" DATE,
    "desart" VARCHAR(2000),
    "relart" NUMERIC(14,2),
    "unimed" VARCHAR(50),
    "codpar" VARCHAR(32),
    "partida" VARCHAR(20),
    "refcom" VARCHAR(8),
    "reqart" VARCHAR(8),
    "indice" VARCHAR(6),
    "codcen" VARCHAR(4),
    "codunimed" VARCHAR(6),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartord"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartreq
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartreq" CASCADE;

CREATE TABLE "caartreq"
(
    "reqart" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(16) NOT NULL,
    "canreq" NUMERIC(14,2),
    "canrec" NUMERIC(14,2),
    "montot" NUMERIC(14,2),
    "unimed" VARCHAR(50),
    "relart" NUMERIC(14,2),
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartreq"."relart" IS 'null';

COMMENT ON COLUMN "caartreq"."codalm" IS 'null';

COMMENT ON COLUMN "caartreq"."codubi" IS 'null';

COMMENT ON COLUMN "caartreq"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartsol
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartsol" CASCADE;

CREATE TABLE "caartsol"
(
    "reqart" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "desart" VARCHAR(2000),
    "codcat" VARCHAR(32) NOT NULL,
    "canreq" NUMERIC(14,2),
    "canrec" NUMERIC(14,2),
    "montot" NUMERIC(14,6),
    "costo" NUMERIC(14,6),
    "monrgo" NUMERIC(14,4),
    "canord" NUMERIC(14,2),
    "mondes" NUMERIC(14,2),
    "relart" NUMERIC(14,2),
    "unimed" VARCHAR(50),
    "codpar" VARCHAR(32),
    "codcen" VARCHAR(4),
    "codunimed" VARCHAR(6),
    "observ" VARCHAR(2000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartsol"."observ" IS 'Se graba las Observaciones';

COMMENT ON COLUMN "caartsol"."id" IS 'Identificador Único del registro';

CREATE INDEX "i_caartsol" ON "caartsol" ("codart");

-----------------------------------------------------------------------
-- caconpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caconpag" CASCADE;

CREATE TABLE "caconpag"
(
    "codconpag" VARCHAR(4) NOT NULL,
    "desconpag" VARCHAR(255) NOT NULL,
    "tipconpag" VARCHAR(1),
    "numdia" NUMERIC(4,0),
    "generaop" VARCHAR(1),
    "asiparrec" VARCHAR(1),
    "generacom" VARCHAR(1),
    "mercon" VARCHAR(20),
    "ctadev" VARCHAR(32),
    "ctavco" VARCHAR(32),
    "univta" VARCHAR(16),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_caconpag_codconpag" UNIQUE ("codconpag")
);

COMMENT ON COLUMN "caconpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cacorrel
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cacorrel" CASCADE;

CREATE TABLE "cacorrel"
(
    "corcom" NUMERIC(8,0),
    "corser" NUMERIC(8,0),
    "corsol" NUMERIC(8,0),
    "correq" NUMERIC(8,0),
    "correc" NUMERIC(8,0),
    "cordes" NUMERIC(8,0),
    "corcot" NUMERIC(8,0),
    "cortra" NUMERIC(8,0),
    "corent" NUMERIC(8,0),
    "corsal" NUMERIC(8,0),
    "corpro" NUMERIC(8,0),
    "corpag" NUMERIC(8,0),
    "corcont" NUMERIC(8,0),
    "corsergen" NUMERIC(8,0),
    "corcomext" NUMERIC(8,0),
    "corsolser" NUMERIC(8,0),
    "corsolman" NUMERIC(8,0),
    "corordman" NUMERIC(8,0),
    "corocmext" NUMERIC(8,0),
    "corsolcot" NUMERIC(8,0),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cacorrel"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefalm" CASCADE;

CREATE TABLE "cadefalm"
(
    "codalm" VARCHAR(20) NOT NULL,
    "nomalm" VARCHAR(100) NOT NULL,
    "codzon" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(16),
    "codtip" INTEGER,
    "catipalm_id" INTEGER,
    "diralm" VARCHAR(500),
    "codalt" VARCHAR(20),
    "codedo" VARCHAR(20),
    "esptoven" BOOLEAN,
    "codtippv" VARCHAR(3),
    "codcta" VARCHAR(32),
    "codemp" VARCHAR(16),
    "tipreg" VARCHAR(1),
    "unicor" VARCHAR(1),
    "corfac" NUMERIC(16,0),
    "cornumctr" NUMERIC(16,0),
    "id" INTEGER NOT NULL,
    "codalmsap" VARCHAR(50),
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefalm"."codzon" IS 'Codigo de la Zona';

COMMENT ON COLUMN "cadefalm"."unicor" IS 'Manejo de Correlativo Único por Almacén (S) o (N)';

COMMENT ON COLUMN "cadefalm"."corfac" IS 'Correlativo Único de Factura por Almacén';

COMMENT ON COLUMN "cadefalm"."cornumctr" IS 'Correlativo Único de Número de Control por Almacén';

COMMENT ON COLUMN "cadefalm"."id" IS 'Identificador Único del registro';

COMMENT ON COLUMN "cadefalm"."codalmsap" IS 'Codigo del Almacén en Sap';

-----------------------------------------------------------------------
-- cadefubi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefubi" CASCADE;

CREATE TABLE "cadefubi"
(
    "codubi" VARCHAR(20) NOT NULL,
    "nomubi" VARCHAR(100) NOT NULL,
    "tipreg" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_cadefubi_codubi" UNIQUE ("codubi")
);

COMMENT ON COLUMN "cadefubi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadescto
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadescto" CASCADE;

CREATE TABLE "cadescto"
(
    "coddesc" VARCHAR(4) NOT NULL,
    "desdesc" VARCHAR(100),
    "tipdesc" VARCHAR(1) NOT NULL,
    "mondesc" NUMERIC(14,2) NOT NULL,
    "diasapl" NUMERIC(4,0) NOT NULL,
    "codcta" VARCHAR(32),
    "tipret" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadescto"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetcot
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetcot" CASCADE;

CREATE TABLE "cadetcot"
(
    "refcot" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "canord" NUMERIC(14,2),
    "costo" NUMERIC(14,6),
    "totdet" NUMERIC(14,6),
    "fecent" DATE,
    "priori" NUMERIC(3,0),
    "justifica" VARCHAR(1000),
    "mondes" NUMERIC(14,2),
    "observaciones" VARCHAR(1000),
    "desart" VARCHAR(2000),
    "codcat" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadetcot"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetordc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetordc" CASCADE;

CREATE TABLE "cadetordc"
(
    "ordcon" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(32) NOT NULL,
    "descon" VARCHAR(1000),
    "moncon" NUMERIC(14,2),
    "cancon" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadetordc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadettra
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadettra" CASCADE;

CREATE TABLE "cadettra"
(
    "codtra" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "canart" NUMERIC(14,2),
    "canrec" NUMERIC(14,2),
    "candev" NUMERIC(14,2),
    "candif" NUMERIC(14,2),
    "codfal" VARCHAR(3),
    "fecest" DATE,
    "obstra" VARCHAR(250),
    "numlotori" VARCHAR(100),
    "numlotdes" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadettra"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadisrgo
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadisrgo" CASCADE;

CREATE TABLE "cadisrgo"
(
    "reqart" VARCHAR(8) NOT NULL,
    "codcat" VARCHAR(50) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codrgo" VARCHAR(4) NOT NULL,
    "monrgo" NUMERIC(14,4),
    "tipdoc" VARCHAR(4),
    "codpre" VARCHAR(50),
    "tipo" VARCHAR(1),
    "desart" VARCHAR(2000),
    "refsol" VARCHAR(8),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadisrgo"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadphart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadphart" CASCADE;

CREATE TABLE "cadphart"
(
    "dphart" VARCHAR(15) NOT NULL,
    "fecdph" DATE NOT NULL,
    "reqart" VARCHAR(8) NOT NULL,
    "desdph" VARCHAR(100),
    "codori" VARCHAR(16),
    "stadph" VARCHAR(1),
    "numcom" VARCHAR(8),
    "refpag" VARCHAR(8),
    "codalm" VARCHAR(20),
    "tipdph" VARCHAR(2),
    "codcli" VARCHAR(10),
    "mondph" NUMERIC(16,2),
    "obsdph" VARCHAR(250),
    "fordesp" VARCHAR(4),
    "reapor" VARCHAR(30),
    "fecanu" DATE,
    "codubi" VARCHAR(20),
    "tipref" VARCHAR(1),
    "codcen" VARCHAR(4),
    "fecemiov" DATE,
    "feccarov" DATE,
    "locori" VARCHAR(250),
    "direccion" VARCHAR(1000),
    "rubro" VARCHAR(100),
    "cankg" NUMERIC(10,2),
    "totpasreal" NUMERIC(10,2),
    "locrec" VARCHAR(250),
    "emptra" VARCHAR(500),
    "nomrep" VARCHAR(250),
    "telemp" VARCHAR(100),
    "choveh" VARCHAR(250),
    "cedcho" VARCHAR(20),
    "telcho" VARCHAR(100),
    "nomconfordes" VARCHAR(250),
    "cedconfordes" VARCHAR(20),
    "horsalconfordes" VARCHAR(20),
    "nomconforrec" VARCHAR(250),
    "cedconforrec" VARCHAR(20),
    "horlleconforrec" VARCHAR(20),
    "codalmusu" VARCHAR(6),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "created_by_user" VARCHAR(50),
    "updated_by_user" VARCHAR(50),
    "fecdes" DATE,
    "fechas" DATE,
    "codedo" VARCHAR(4),
    "codciu" VARCHAR(4),
    "codmun" VARCHAR(4),
    "codpar" VARCHAR(4),
    "codinst" VARCHAR(8),
    "codprg" VARCHAR(6),
    "stadev" VARCHAR(1),
    "codpro" VARCHAR(15),
    "coddirec" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadphart"."codalmusu" IS 'Enlace con la tabla causualm';

COMMENT ON COLUMN "cadphart"."fecdes" IS 'Fecha desde PAE';

COMMENT ON COLUMN "cadphart"."fechas" IS 'Fecha hasta PAE';

COMMENT ON COLUMN "cadphart"."codedo" IS 'Codigo del Estado';

COMMENT ON COLUMN "cadphart"."codciu" IS 'Codigo de la Ciudad';

COMMENT ON COLUMN "cadphart"."codmun" IS 'Codigo del Municipio';

COMMENT ON COLUMN "cadphart"."codpar" IS 'Codigo de la Parroquia';

COMMENT ON COLUMN "cadphart"."codinst" IS 'Código de la institución';

COMMENT ON COLUMN "cadphart"."codprg" IS 'Código del Programa';

COMMENT ON COLUMN "cadphart"."stadev" IS 'estatus de devolucion S o N';

COMMENT ON COLUMN "cadphart"."codpro" IS 'Codigo del proveedor que Despacha';

COMMENT ON COLUMN "cadphart"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cadphart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadphartser
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadphartser" CASCADE;

CREATE TABLE "cadphartser"
(
    "dphart" VARCHAR(8) NOT NULL,
    "fecdph" DATE NOT NULL,
    "reqart" VARCHAR(8) NOT NULL,
    "desdph" VARCHAR(255),
    "codori" VARCHAR(16),
    "stadph" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadphartser"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caforent
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caforent" CASCADE;

CREATE TABLE "caforent"
(
    "codforent" VARCHAR(4) NOT NULL,
    "desforent" VARCHAR(255) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_caforent_codforent" UNIQUE ("codforent")
);

COMMENT ON COLUMN "caforent"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- camotfal
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "camotfal" CASCADE;

CREATE TABLE "camotfal"
(
    "codfal" VARCHAR(3) NOT NULL,
    "desfal" VARCHAR(250) NOT NULL,
    "tipfal" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "camotfal"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caordcon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caordcon" CASCADE;

CREATE TABLE "caordcon"
(
    "ordcon" VARCHAR(8) NOT NULL,
    "feccon" DATE NOT NULL,
    "tipcon" VARCHAR(4) NOT NULL,
    "codpro" VARCHAR(15) NOT NULL,
    "descon" VARCHAR(1000),
    "objcon" VARCHAR(1000),
    "fecini" DATE,
    "feccul" DATE,
    "mulatrini" NUMERIC(14,2),
    "lapgar" NUMERIC(14,2),
    "mulatrcul" NUMERIC(14,2),
    "stacon" VARCHAR(1),
    "moncon" NUMERIC(14,2),
    "fecanu" DATE,
    "cancuo" NUMERIC(3,0),
    "fecpto" DATE,
    "numcon" VARCHAR(25),
    "numpto" VARCHAR(50),
    "numres" VARCHAR(50),
    "otorga" VARCHAR(1),
    "fecfir" DATE,
    "otoant" BOOLEAN,
    "monoto" NUMERIC(14,2),
    "pamopag" NUMERIC(14,2),
    "cumresp" BOOLEAN,
    "otoesp" BOOLEAN,
    "monaes" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caordcon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caordconpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caordconpag" CASCADE;

CREATE TABLE "caordconpag"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "codconpag" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caordconpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caordforent
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caordforent" CASCADE;

CREATE TABLE "caordforent"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "codforent" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caordforent"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caramart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caramart" CASCADE;

CREATE TABLE "caramart"
(
    "ramart" VARCHAR(6) NOT NULL,
    "nomram" VARCHAR(100) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caramart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carancot
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carancot" CASCADE;

CREATE TABLE "carancot"
(
    "candes" NUMERIC(14,2) NOT NULL,
    "canhas" NUMERIC(14,2) NOT NULL,
    "cancot" NUMERIC(14,2) NOT NULL,
    "nroran" VARCHAR(2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "carancot"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carazcom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carazcom" CASCADE;

CREATE TABLE "carazcom"
(
    "codrazcom" VARCHAR(4) NOT NULL,
    "desrazcom" VARCHAR(255) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_carazcom_codrazcom" UNIQUE ("codrazcom")
);

COMMENT ON COLUMN "carazcom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carcpart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carcpart" CASCADE;

CREATE TABLE "carcpart"
(
    "rcpart" VARCHAR(8) NOT NULL,
    "fecrcp" DATE NOT NULL,
    "ordcom" VARCHAR(8) NOT NULL,
    "desrcp" VARCHAR(100),
    "codpro" VARCHAR(15),
    "numfac" VARCHAR(15),
    "monrcp" NUMERIC(14,2),
    "starcp" VARCHAR(1),
    "numcom" VARCHAR(8),
    "numord" VARCHAR(8),
    "codalm" VARCHAR(20),
    "ctrper" VARCHAR(50),
    "genord" VARCHAR(1),
    "nroent" VARCHAR(8),
    "fecfac" DATE,
    "codubi" VARCHAR(20),
    "nomcli" VARCHAR(100),
    "cancaj" NUMERIC(10,2),
    "canjau" NUMERIC(10,2),
    "codcen" VARCHAR(4),
    "id" INTEGER NOT NULL,
    "codalmusu" VARCHAR(6),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "created_by_user" VARCHAR(50),
    "updated_by_user" VARCHAR(50),
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "carcpart"."id" IS 'Identificador Único del registro';

COMMENT ON COLUMN "carcpart"."codalmusu" IS 'Enlace con la tabla causualm';

-----------------------------------------------------------------------
-- carecarg
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carecarg" CASCADE;

CREATE TABLE "carecarg"
(
    "codrgo" VARCHAR(4) NOT NULL,
    "nomrgo" VARCHAR(100) NOT NULL,
    "codpre" VARCHAR(32) NOT NULL,
    "tiprgo" VARCHAR(1) NOT NULL,
    "monrgo" NUMERIC(14,2) NOT NULL,
    "calcul" VARCHAR(1),
    "codcta" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_carecarg_codrgo" UNIQUE ("codrgo")
);

COMMENT ON COLUMN "carecarg"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carecaud
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carecaud" CASCADE;

CREATE TABLE "carecaud"
(
    "codrec" VARCHAR(10) NOT NULL,
    "desrec" VARCHAR(100) NOT NULL,
    "limrec" VARCHAR(1),
    "fecemi" DATE,
    "fecven" DATE,
    "canutr" NUMERIC(14,2),
    "codtiprec" VARCHAR(4) NOT NULL,
    "observ" VARCHAR(100),
    "puntua" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_carecaud_codrec" UNIQUE ("codrec")
);

COMMENT ON COLUMN "carecaud"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carecpro
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carecpro" CASCADE;

CREATE TABLE "carecpro"
(
    "codpro" VARCHAR(15) NOT NULL,
    "codrec" VARCHAR(10) NOT NULL,
    "fecent" DATE,
    "fecven" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "carecpro"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- careqart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "careqart" CASCADE;

CREATE TABLE "careqart"
(
    "reqart" VARCHAR(8) NOT NULL,
    "fecreq" DATE NOT NULL,
    "desreq" VARCHAR(500),
    "monreq" NUMERIC(14,2),
    "stareq" VARCHAR(1),
    "unisol" VARCHAR(4),
    "codcatreq" VARCHAR(32),
    "aprreq" VARCHAR(1),
    "nummemo" VARCHAR(10),
    "justif" VARCHAR(250),
    "codcen" VARCHAR(4),
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "stadesp" VARCHAR(1),
    "motivo" VARCHAR(500),
    "usuapr" VARCHAR(50),
    "fecapr" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "careqart"."codcen" IS 'null';

COMMENT ON COLUMN "careqart"."codalm" IS 'null';

COMMENT ON COLUMN "careqart"."codubi" IS 'null';

COMMENT ON COLUMN "careqart"."stadesp" IS 'Estatus actualizado desde despacho';

COMMENT ON COLUMN "careqart"."usuapr" IS 'usuario que realizo la aprobación';

COMMENT ON COLUMN "careqart"."fecapr" IS 'fecha en que realizo la aprobación';

COMMENT ON COLUMN "careqart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- careqartser
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "careqartser" CASCADE;

CREATE TABLE "careqartser"
(
    "reqart" VARCHAR(8) NOT NULL,
    "fecreq" DATE NOT NULL,
    "desreq" VARCHAR(255),
    "stareq" VARCHAR(1),
    "codcatreq" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "careqartser"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caresordcom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caresordcom" CASCADE;

CREATE TABLE "caresordcom"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "desres" VARCHAR(500),
    "codartpro" VARCHAR(20),
    "canord" NUMERIC(14,2),
    "canaju" NUMERIC(14,2),
    "canrec" NUMERIC(14,2),
    "cantot" NUMERIC(14,2),
    "costo" NUMERIC(14,6),
    "rgoart" NUMERIC(14,4),
    "totart" NUMERIC(14,6),
    "codart" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caresordcom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caretser
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caretser" CASCADE;

CREATE TABLE "caretser"
(
    "codser" VARCHAR(20) NOT NULL,
    "codret" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caretser"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cargosol
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cargosol" CASCADE;

CREATE TABLE "cargosol"
(
    "reqart" VARCHAR(8) NOT NULL,
    "codrgo" VARCHAR(4) NOT NULL,
    "monrgo" NUMERIC(14,4) NOT NULL,
    "tipdoc" VARCHAR(4) NOT NULL,
    "tipo" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cargosol"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- casolart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "casolart" CASCADE;

CREATE TABLE "casolart"
(
    "reqart" VARCHAR(8) NOT NULL,
    "fecreq" DATE NOT NULL,
    "desreq" VARCHAR(1000),
    "monreq" NUMERIC(14,2),
    "stareq" VARCHAR(1),
    "motreq" VARCHAR(1000),
    "benreq" VARCHAR(1000),
    "mondes" NUMERIC(14,2),
    "obsreq" VARCHAR(5000),
    "unires" VARCHAR(32),
    "tipmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "fecanu" DATE,
    "codpro" VARCHAR(15),
    "reqcom" VARCHAR(8),
    "tipfin" VARCHAR(4),
    "tipreq" CHAR(1),
    "aprreq" CHAR(1),
    "usuapr" CHAR(1),
    "fecapr" DATE,
    "codemp" VARCHAR(16),
    "codcen" VARCHAR(4),
    "numproc" VARCHAR(30),
    "codeve" VARCHAR(6),
    "coddirec" VARCHAR(4),
    "coddivi" VARCHAR(6),
    "loguse" VARCHAR(50),
    "fecana" DATE,
    "codubi" VARCHAR(30),
    "nomben" VARCHAR(250),
    "cedrif" VARCHAR(15),
    "fecsal" DATE,
    "horsal" VARCHAR(10),
    "fecreg" DATE,
    "horreg" VARCHAR(10),
    "codreg" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "casolart"."codeve" IS 'Codigo del Evento';

COMMENT ON COLUMN "casolart"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "casolart"."coddivi" IS 'Código de la División';

COMMENT ON COLUMN "casolart"."loguse" IS 'login del usuario que hizo la SC';

COMMENT ON COLUMN "casolart"."fecana" IS 'Fecha en que se hizo el análisis de Cotización';

COMMENT ON COLUMN "casolart"."codubi" IS 'Unidad Responsable de Bnubica.';

COMMENT ON COLUMN "casolart"."nomben" IS 'Nombre del Pasajero';

COMMENT ON COLUMN "casolart"."cedrif" IS 'Cédula o RIF del Pasajero';

COMMENT ON COLUMN "casolart"."fecsal" IS 'Fecha de Salida';

COMMENT ON COLUMN "casolart"."horsal" IS 'Hora de Salida';

COMMENT ON COLUMN "casolart"."fecreg" IS 'Fecha de Regreso';

COMMENT ON COLUMN "casolart"."horreg" IS 'Hora de Regreso';

COMMENT ON COLUMN "casolart"."codreg" IS 'Código de la Región';

COMMENT ON COLUMN "casolart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catalogo
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catalogo" CASCADE;

CREATE TABLE "catalogo"
(
    "codcta" VARCHAR(18) NOT NULL,
    "descta" VARCHAR(40) NOT NULL,
    "fecini" DATE NOT NULL,
    "feccie" DATE NOT NULL,
    "salant" NUMERIC(12,2),
    "debcre" VARCHAR(1) NOT NULL,
    "cargab" VARCHAR(1) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "catalogo"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catipent
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catipent" CASCADE;

CREATE TABLE "catipent"
(
    "codtipent" VARCHAR(3) NOT NULL,
    "destipent" VARCHAR(50) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_catipent_codtipent" UNIQUE ("codtipent")
);

COMMENT ON COLUMN "catipent"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catippro
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catippro" CASCADE;

CREATE TABLE "catippro"
(
    "codpro" VARCHAR(4) NOT NULL,
    "despro" VARCHAR(100) NOT NULL,
    "ctaord" VARCHAR(32),
    "ctaper" VARCHAR(32),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "catippro"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catiprec
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catiprec" CASCADE;

CREATE TABLE "catiprec"
(
    "codtiprec" VARCHAR(4) NOT NULL,
    "destiprec" VARCHAR(100) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_catiprec_codtiprec" UNIQUE ("codtiprec")
);

COMMENT ON COLUMN "catiprec"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catipsal
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catipsal" CASCADE;

CREATE TABLE "catipsal"
(
    "codtipsal" VARCHAR(3) NOT NULL,
    "destipsal" VARCHAR(50) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "unique_catipsal_codtipsal" UNIQUE ("codtipsal")
);

COMMENT ON COLUMN "catipsal"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartpar
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartpar" CASCADE;

CREATE TABLE "caartpar"
(
    "codart" VARCHAR(20) NOT NULL,
    "codpar" VARCHAR(16),
    "porpar" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartpar"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caregart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caregart" CASCADE;

CREATE TABLE "caregart"
(
    "codart" VARCHAR(20) NOT NULL,
    "desart" VARCHAR(1500) NOT NULL,
    "codcta" VARCHAR(32),
    "codpar" VARCHAR(16),
    "ramart" VARCHAR(6),
    "cosult" NUMERIC(14,2),
    "cospro" NUMERIC(14,2),
    "exitot" NUMERIC(14,2),
    "unimed" VARCHAR(15),
    "unialt" VARCHAR(15),
    "relart" VARCHAR(25),
    "fecult" DATE,
    "invini" NUMERIC(14,2),
    "codmar" VARCHAR(6),
    "codref" VARCHAR(15),
    "costot" NUMERIC(14,2),
    "sigecof" VARCHAR(20),
    "codclaart" NUMERIC(4,0),
    "lotuni" VARCHAR(1),
    "ctavta" VARCHAR(32),
    "ctacos" VARCHAR(32),
    "ctapro" VARCHAR(32),
    "preart" VARCHAR(50),
    "distot" NUMERIC(14,2),
    "tipo" VARCHAR(1),
    "tip0" VARCHAR(1),
    "coding" VARCHAR(32),
    "mercon" VARCHAR(1),
    "codartsnc" VARCHAR(20),
    "tipreg" VARCHAR(1),
    "perbienes" BOOLEAN,
    "ctatra" VARCHAR(32),
    "cosunipri" NUMERIC(14,2),
    "ctadef" VARCHAR(32),
    "tippro" VARCHAR(1),
    "codbar" VARCHAR(32),
    "nacimp" VARCHAR(1),
    "codalt" VARCHAR(20),
    "staart" VARCHAR(1),
    "codunimed" VARCHAR(6),
    "claart" VARCHAR(1),
    "nomcom" VARCHAR(250),
    "cosact" NUMERIC(14,2),
    "feccac" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caregart"."claart" IS 'Clasificación del Artículo Regional ó Central';

COMMENT ON COLUMN "caregart"."nomcom" IS 'Nombre Comercial';

COMMENT ON COLUMN "caregart"."cosact" IS 'Costo actual en el mercado';

COMMENT ON COLUMN "caregart"."feccac" IS 'Fecha del Costo actual';

COMMENT ON COLUMN "caregart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caordcom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caordcom" CASCADE;

CREATE TABLE "caordcom"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "fecord" DATE NOT NULL,
    "codpro" VARCHAR(15) NOT NULL,
    "desord" VARCHAR(2500),
    "crecon" VARCHAR(2),
    "plaent" VARCHAR(25),
    "tiecan" VARCHAR(25),
    "monord" NUMERIC(14,2),
    "dtoord" NUMERIC(14,2),
    "refcom" VARCHAR(8),
    "staord" VARCHAR(1),
    "afepre" VARCHAR(1),
    "conpag" VARCHAR(1000) NOT NULL,
    "forent" VARCHAR(1000) NOT NULL,
    "fecanu" DATE,
    "tipmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "tipcom" VARCHAR(1),
    "tipord" VARCHAR(1),
    "tipo" VARCHAR(1),
    "coduni" VARCHAR(30),
    "codemp" VARCHAR(16),
    "notord" VARCHAR(1000),
    "tipdoc" VARCHAR(4),
    "tippro" VARCHAR(4),
    "afepro" VARCHAR(1),
    "doccom" VARCHAR(4),
    "refsol" VARCHAR(1000),
    "tipfin" VARCHAR(4),
    "justif" VARCHAR(1000),
    "refprc" VARCHAR(1),
    "codmedcom" VARCHAR(4),
    "codprocom" VARCHAR(4),
    "codpai" VARCHAR(4),
    "codedo" VARCHAR(4),
    "codmun" VARCHAR(4),
    "aplart" VARCHAR(2),
    "aplart6" VARCHAR(2),
    "numsigecof" VARCHAR(8),
    "fecsigecof" DATE,
    "expsigecof" VARCHAR(8),
    "codcen" VARCHAR(4),
    "tipocom" VARCHAR(50),
    "ceddon" VARCHAR(15),
    "nomdon" VARCHAR(50),
    "fecdon" DATE,
    "sexdon" VARCHAR(1),
    "edadon" NUMERIC(2,0),
    "serdon" VARCHAR(1),
    "motanu" VARCHAR(500),
    "usuanu" VARCHAR(250),
    "codcenaco" VARCHAR(4),
    "numproc" VARCHAR(30),
    "usuroc" VARCHAR(50),
    "fecrec" DATE,
    "enviad" VARCHAR(1),
    "fecenv" DATE,
    "stapda" VARCHAR(1),
    "numcon" VARCHAR(20),
    "coddirec" VARCHAR(4),
    "coddivi" VARCHAR(6),
    "codeve" VARCHAR(6),
    "lugfec" VARCHAR(250),
    "dirent" VARCHAR(250),
    "numpro" VARCHAR(30),
    "staver" VARCHAR(1),
    "percon" VARCHAR(500),
    "telcon" VARCHAR(50),
    "faxcon" VARCHAR(15),
    "emacon" VARCHAR(50),
    "codgar" VARCHAR(3),
    "usuapr" VARCHAR(50),
    "fecapr" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caordcom"."tipocom" IS 'OC Donación Tipo de Compra';

COMMENT ON COLUMN "caordcom"."ceddon" IS 'OC Donación Cédula';

COMMENT ON COLUMN "caordcom"."nomdon" IS 'OC Donación Nombre';

COMMENT ON COLUMN "caordcom"."fecdon" IS 'OC Donación Fecha de Nacimiento';

COMMENT ON COLUMN "caordcom"."sexdon" IS 'OC Donación Sexo';

COMMENT ON COLUMN "caordcom"."edadon" IS 'OC Donación Edad';

COMMENT ON COLUMN "caordcom"."serdon" IS 'OC Donación Servicio Prestado Medicina Materiales Otro';

COMMENT ON COLUMN "caordcom"."fecrec" IS 'Fecha de Recepcion en Correo';

COMMENT ON COLUMN "caordcom"."enviad" IS 'Recibido o enviado';

COMMENT ON COLUMN "caordcom"."fecenv" IS 'Fecha de Envio del Correo';

COMMENT ON COLUMN "caordcom"."stapda" IS 'Indica si esta asociada a un PDA';

COMMENT ON COLUMN "caordcom"."numcon" IS 'Numero de Contrato';

COMMENT ON COLUMN "caordcom"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "caordcom"."coddivi" IS 'Código de la División';

COMMENT ON COLUMN "caordcom"."codeve" IS 'Codigo del Evento';

COMMENT ON COLUMN "caordcom"."lugfec" IS 'Lugar y Fecha de Entrega';

COMMENT ON COLUMN "caordcom"."dirent" IS 'Dirección de Entrega';

COMMENT ON COLUMN "caordcom"."numpro" IS 'N° de Proceso';

COMMENT ON COLUMN "caordcom"."staver" IS 'Estatus de Verificado S ó N';

COMMENT ON COLUMN "caordcom"."percon" IS 'Persona Contacto';

COMMENT ON COLUMN "caordcom"."telcon" IS 'Teléfono Persona Contacto';

COMMENT ON COLUMN "caordcom"."faxcon" IS 'Fax Persona Contacto';

COMMENT ON COLUMN "caordcom"."emacon" IS 'Email Persona Contacto';

COMMENT ON COLUMN "caordcom"."codgar" IS 'Código de la Garantía';

COMMENT ON COLUMN "caordcom"."usuapr" IS 'usuario que realizo la aprobación';

COMMENT ON COLUMN "caordcom"."fecapr" IS 'fecha en que realizo la aprobación';

COMMENT ON COLUMN "caordcom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cacatsnc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cacatsnc" CASCADE;

CREATE TABLE "cacatsnc"
(
    "codsnc" VARCHAR(20) NOT NULL,
    "dessnc" VARCHAR(1000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cacatsnc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catipempsnc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catipempsnc" CASCADE;

CREATE TABLE "catipempsnc"
(
    "codtip" VARCHAR(4) NOT NULL,
    "destip" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "catipempsnc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- camedcom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "camedcom" CASCADE;

CREATE TABLE "camedcom"
(
    "codmedcom" VARCHAR(4) NOT NULL,
    "desmedcom" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "camedcom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caprocom
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caprocom" CASCADE;

CREATE TABLE "caprocom"
(
    "codprocom" VARCHAR(4) NOT NULL,
    "desprocom" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caprocom"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caprovee
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caprovee" CASCADE;

CREATE TABLE "caprovee"
(
    "codpro" VARCHAR(15) NOT NULL,
    "nompro" VARCHAR(250) NOT NULL,
    "rifpro" VARCHAR(15) NOT NULL,
    "nitpro" VARCHAR(15),
    "dirpro" VARCHAR(100),
    "telpro" VARCHAR(30),
    "faxpro" VARCHAR(15),
    "email" VARCHAR(100),
    "limcre" NUMERIC(14,2),
    "codcta" VARCHAR(32),
    "regmer" VARCHAR(15),
    "fecreg" DATE,
    "tomreg" VARCHAR(15),
    "folreg" VARCHAR(15),
    "capsus" NUMERIC(14,2),
    "cappag" NUMERIC(14,2),
    "rifrepleg" VARCHAR(15),
    "nomrepleg" VARCHAR(50),
    "dirrepleg" VARCHAR(100),
    "telrepleg" VARCHAR(30),
    "nrocei" VARCHAR(30),
    "codram" VARCHAR(6),
    "fecinscir" DATE,
    "numinscir" VARCHAR(20),
    "nacpro" VARCHAR(1),
    "codord" VARCHAR(32),
    "codpercon" VARCHAR(32),
    "codtiprec" VARCHAR(4),
    "codordadi" VARCHAR(32),
    "codperconadi" VARCHAR(32),
    "tipo" VARCHAR(1),
    "fecven" DATE,
    "ciudad" VARCHAR(100),
    "codordmercon" VARCHAR(32),
    "codpermercon" VARCHAR(32),
    "codordcontra" VARCHAR(32),
    "codpercontra" VARCHAR(32),
    "temcodpro" VARCHAR(10),
    "temrifpro" VARCHAR(15),
    "codctasec" VARCHAR(32),
    "codtipemp" VARCHAR(4),
    "ramgen" VARCHAR(1000),
    "estpro" VARCHAR(1),
    "codban" VARCHAR(4),
    "numcue" VARCHAR(20),
    "codtip" VARCHAR(3),
    "estapo" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "u_caprovee" UNIQUE ("rifpro")
);

COMMENT ON COLUMN "caprovee"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caprocomart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caprocomart" CASCADE;

CREATE TABLE "caprocomart"
(
    "fecprocom" DATE,
    "canpro" NUMERIC(14,2),
    "monpro" NUMERIC(14,2),
    "mespro" VARCHAR(2),
    "codedo" VARCHAR(4),
    "codcat" VARCHAR(32),
    "codciu" VARCHAR(4),
    "codmun" VARCHAR(4),
    "codfin" VARCHAR(4),
    "codart" VARCHAR(20),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caprocomart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefart" CASCADE;

CREATE TABLE "cadefart"
(
    "codemp" VARCHAR(3) NOT NULL,
    "lonart" NUMERIC(2,0) NOT NULL,
    "rupart" NUMERIC(2,0) NOT NULL,
    "forart" VARCHAR(30) NOT NULL,
    "desart" VARCHAR(30) NOT NULL,
    "forubi" VARCHAR(30) NOT NULL,
    "desubi" VARCHAR(30) NOT NULL,
    "correq" VARCHAR(8),
    "corord" VARCHAR(8),
    "correc" VARCHAR(8),
    "cordes" VARCHAR(8),
    "generaop" VARCHAR(1),
    "asiparrec" VARCHAR(1),
    "generacom" VARCHAR(1),
    "mercon" VARCHAR(20),
    "ctadev" VARCHAR(32),
    "ctavco" VARCHAR(32),
    "univta" VARCHAR(32),
    "artven" VARCHAR(20),
    "artins" VARCHAR(20),
    "artser" VARCHAR(20),
    "codalmven" VARCHAR(20),
    "recart" NUMERIC(8,0),
    "ordcom" NUMERIC(8,0),
    "reqart" NUMERIC(8,0),
    "dphart" NUMERIC(8,0),
    "ordser" NUMERIC(8,0),
    "tipimprec" VARCHAR(1),
    "artvenhas" VARCHAR(20),
    "corcot" VARCHAR(8),
    "solart" VARCHAR(8),
    "apliclades" VARCHAR(1),
    "solcom" NUMERIC(8,0),
    "unidad" VARCHAR(100),
    "prcasopre" VARCHAR(1),
    "prcreqapr" VARCHAR(1),
    "comasopre" VARCHAR(1),
    "comreqapr" VARCHAR(1),
    "almcorre" VARCHAR(8) NOT NULL,
    "forsnc" VARCHAR(20),
    "dessnc" VARCHAR(20),
    "reqreqapr" VARCHAR(1),
    "solreqapr" VARCHAR(1),
    "gencorart" VARCHAR(1),
    "tipdocpre" VARCHAR(4),
    "cornac" INTEGER,
    "corext" INTEGER,
    "tipodoc" VARCHAR(4),
    "codconpag" VARCHAR(4),
    "codforent" VARCHAR(4),
    "forpro" VARCHAR(15),
    "despro" VARCHAR(15),
    "tipfin" VARCHAR(4),
    "codmon" VARCHAR(3),
    "reppreimpsc" VARCHAR(50),
    "reppreimpoc" VARCHAR(50),
    "codtiptra" VARCHAR(3),
    "percon" VARCHAR(250),
    "faxcon" VARCHAR(30),
    "emacon" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefart"."tipodoc" IS 'Se usa para definir el tipo de OC de Donación';

COMMENT ON COLUMN "cadefart"."codtiptra" IS 'Tipo de Transación';

COMMENT ON COLUMN "cadefart"."percon" IS 'Persona Contacto';

COMMENT ON COLUMN "cadefart"."faxcon" IS 'Fax';

COMMENT ON COLUMN "cadefart"."emacon" IS 'Email';

COMMENT ON COLUMN "cadefart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cacotiza
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cacotiza" CASCADE;

CREATE TABLE "cacotiza"
(
    "refcot" VARCHAR(8) NOT NULL,
    "feccot" DATE NOT NULL,
    "codpro" VARCHAR(15) NOT NULL,
    "descot" VARCHAR(1000),
    "refsol" VARCHAR(8) NOT NULL,
    "moncot" NUMERIC(14,2),
    "conpag" VARCHAR(4),
    "forent" VARCHAR(4),
    "priori" NUMERIC(3,0),
    "mondes" NUMERIC(14,2),
    "monrec" NUMERIC(14,4),
    "tipmon" VARCHAR(3) NOT NULL,
    "valmon" NUMERIC(14,6),
    "refpro" VARCHAR(10),
    "tipo" VARCHAR(1),
    "correl" NUMERIC(3,0),
    "porvan" NUMERIC(14,2),
    "porant" NUMERIC(14,2),
    "obscot" VARCHAR(2000),
    "numproc" VARCHAR(30),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cacotiza"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartalmubi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartalmubi" CASCADE;

CREATE TABLE "caartalmubi"
(
    "codalm" VARCHAR(20) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "exiact" NUMERIC(14,2),
    "numlot" VARCHAR(100),
    "fecela" DATE,
    "fecven" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartalmubi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caalmubi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caalmubi" CASCADE;

CREATE TABLE "caalmubi"
(
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "tipreg" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caalmubi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caentalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caentalm" CASCADE;

CREATE TABLE "caentalm"
(
    "rcpart" VARCHAR(8) NOT NULL,
    "fecrcp" DATE NOT NULL,
    "desrcp" VARCHAR(100),
    "codpro" VARCHAR(15),
    "monrcp" NUMERIC(14,2),
    "starcp" VARCHAR(1),
    "codalm" VARCHAR(20),
    "codubi" VARCHAR(20),
    "tipmov" VARCHAR(3) NOT NULL,
    "codcen" VARCHAR(4),
    "dphart" VARCHAR(15),
    "numcom" VARCHAR(8),
    "codalmusu" VARCHAR(6),
    "codsada" VARCHAR(20),
    "nroentdes" VARCHAR(20),
    "nrocarveh" VARCHAR(20),
    "nrocontro" VARCHAR(20),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "created_by_user" VARCHAR(50),
    "updated_by_user" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caentalm"."codalmusu" IS 'Enlace con la tabla causualm';

COMMENT ON COLUMN "caentalm"."codsada" IS 'Código SADA';

COMMENT ON COLUMN "caentalm"."nroentdes" IS 'Número de Nota de Entrega-Despacho';

COMMENT ON COLUMN "caentalm"."nrocarveh" IS 'Número de Orden de Carga Vehicular';

COMMENT ON COLUMN "caentalm"."nrocontro" IS 'Número de Control';

COMMENT ON COLUMN "caentalm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- casalalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "casalalm" CASCADE;

CREATE TABLE "casalalm"
(
    "codsal" VARCHAR(8) NOT NULL,
    "fecsal" DATE NOT NULL,
    "dessal" VARCHAR(100),
    "codpro" VARCHAR(15),
    "monsal" NUMERIC(14,2),
    "stasal" VARCHAR(1),
    "codalm" VARCHAR(20),
    "codubi" VARCHAR(20),
    "tipmov" VARCHAR(3) NOT NULL,
    "observ" VARCHAR(1000),
    "codcen" VARCHAR(4),
    "reqart" VARCHAR(8),
    "numcom" VARCHAR(8),
    "codalmusu" VARCHAR(6),
    "created_at" TIMESTAMP,
    "updated_at" TIMESTAMP,
    "created_by_user" VARCHAR(50),
    "updated_by_user" VARCHAR(50),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "casalalm"."codalmusu" IS 'Enlace con la tabla causualm';

COMMENT ON COLUMN "casalalm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cainvfis
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cainvfis" CASCADE;

CREATE TABLE "cainvfis"
(
    "fecinv" DATE NOT NULL,
    "codalm" VARCHAR(20) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "exiact" NUMERIC(14,2),
    "exiact2" NUMERIC(14,2),
    "codubi" VARCHAR(20),
    "fectras" DATE,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cainvfis"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catraalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catraalm" CASCADE;

CREATE TABLE "catraalm"
(
    "codtra" VARCHAR(8) NOT NULL,
    "fectra" DATE,
    "destra" VARCHAR(255),
    "almori" VARCHAR(6) NOT NULL,
    "codubiori" VARCHAR(20),
    "almdes" VARCHAR(6) NOT NULL,
    "codubides" VARCHAR(20),
    "statra" VARCHAR(1),
    "obstra" VARCHAR(250),
    "codemptra" VARCHAR(4),
    "fadefveh_id" INTEGER,
    "fadefcho_id" INTEGER,
    "fecsal" TIMESTAMP,
    "feclle" TIMESTAMP,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "catraalm"."codemptra" IS 'null';

COMMENT ON COLUMN "catraalm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- casolraz
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "casolraz" CASCADE;

CREATE TABLE "casolraz"
(
    "numsol" VARCHAR(8) NOT NULL,
    "codrazcom" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "casolraz"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartrcp
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartrcp" CASCADE;

CREATE TABLE "caartrcp"
(
    "rcpart" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "ordcom" VARCHAR(8),
    "codcat" VARCHAR(32) NOT NULL,
    "canrec" NUMERIC(32,8),
    "candev" NUMERIC(14,2),
    "cantot" NUMERIC(14,2),
    "montot" NUMERIC(14,2),
    "monrgo" NUMERIC(14,2),
    "mondes" NUMERIC(14,2),
    "canasilot" NUMERIC(14,2),
    "codfal" VARCHAR(3),
    "fecest" DATE,
    "serial" VARCHAR(20),
    "codalm" VARCHAR(20),
    "codubi" VARCHAR(20),
    "numlot" VARCHAR(100),
    "marca" VARCHAR(20),
    "modelo" VARCHAR(20),
    "desart" VARCHAR(2000),
    "codunimed" VARCHAR(6),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartrcp"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartreqser
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartreqser" CASCADE;

CREATE TABLE "caartreqser"
(
    "reqart" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codcat" VARCHAR(16) NOT NULL,
    "fecrea" DATE,
    "canreq" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caartreqser"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetent
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetent" CASCADE;

CREATE TABLE "cadetent"
(
    "rcpart" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "canrec" NUMERIC(32,8),
    "montot" NUMERIC(14,2),
    "cosart" NUMERIC(14,2),
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "fecven" DATE,
    "numjau" NUMERIC(10,2),
    "tammet" NUMERIC(10,2),
    "numlot" VARCHAR(100),
    "fecela" DATE,
    "codalt" VARCHAR(20),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadetent"."codalt" IS 'Código Alterno(Barra) del Artículo';

COMMENT ON COLUMN "cadetent"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetsal
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetsal" CASCADE;

CREATE TABLE "cadetsal"
(
    "codsal" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "cantot" NUMERIC(32,8),
    "totart" NUMERIC(14,2),
    "cosart" NUMERIC(14,2),
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "numjau" NUMERIC(10,2),
    "tammet" NUMERIC(10,2),
    "numlot" VARCHAR(100),
    "codalt" VARCHAR(20),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadetsal"."codalt" IS 'Código Alterno(Barra) del Artículo';

COMMENT ON COLUMN "cadetsal"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cainvfisubi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cainvfisubi" CASCADE;

CREATE TABLE "cainvfisubi"
(
    "fecinv" DATE NOT NULL,
    "codalm" VARCHAR(20) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20) NOT NULL,
    "exiact" NUMERIC(14,2) NOT NULL,
    "exiact2" NUMERIC(14,2) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cainvfisubi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catipalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catipalm" CASCADE;

CREATE TABLE "catipalm"
(
    "nomtip" VARCHAR(100) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "catipalm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caconmer
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caconmer" CASCADE;

CREATE TABLE "caconmer"
(
    "conmer" VARCHAR(8) NOT NULL,
    "feccon" DATE NOT NULL,
    "descon" VARCHAR(1000) NOT NULL,
    "codpro" VARCHAR(10) NOT NULL,
    "numdoc" VARCHAR(20) NOT NULL,
    "moncon" NUMERIC(14,2),
    "stacon" VARCHAR(1) NOT NULL,
    "codalm" VARCHAR(20) NOT NULL,
    "numcom" VARCHAR(8) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caconmer"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caproret
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caproret" CASCADE;

CREATE TABLE "caproret"
(
    "codpro" VARCHAR(15) NOT NULL,
    "codret" VARCHAR(3) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caproret"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caconpro
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caconpro" CASCADE;

CREATE TABLE "caconpro"
(
    "codpro" VARCHAR(15) NOT NULL,
    "cedcon" VARCHAR(15) NOT NULL,
    "nomcon" VARCHAR(250),
    "dircon" VARCHAR(250),
    "telcon" VARCHAR(30),
    "emacon" VARCHAR(100),
    "relcon" VARCHAR(2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caconpro"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carampro
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carampro" CASCADE;

CREATE TABLE "carampro"
(
    "codpro" VARCHAR(15) NOT NULL,
    "ramart" VARCHAR(6) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "carampro"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cabanco
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cabanco" CASCADE;

CREATE TABLE "cabanco"
(
    "codban" VARCHAR(4) NOT NULL,
    "desban" VARCHAR(50) NOT NULL,
    "codpagele" VARCHAR(20),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cabanco"."desban" IS 'null';

COMMENT ON COLUMN "cabanco"."codpagele" IS 'Código de Pago Eléctronico';

COMMENT ON COLUMN "cabanco"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- casolpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "casolpag" CASCADE;

CREATE TABLE "casolpag"
(
    "solpag" VARCHAR(8) NOT NULL,
    "fecsol" DATE NOT NULL,
    "dessol" VARCHAR(1000),
    "tipcom" VARCHAR(4) NOT NULL,
    "cedrif" VARCHAR(15),
    "monsol" NUMERIC(14,2) NOT NULL,
    "stasol" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "casolpag"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetpag
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetpag" CASCADE;

CREATE TABLE "cadetpag"
(
    "solpag" VARCHAR(8) NOT NULL,
    "codpre" VARCHAR(32) NOT NULL,
    "moncom" NUMERIC(14,2) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- caunifor
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caunifor" CASCADE;

CREATE TABLE "caunifor"
(
    "codcat" VARCHAR(32) NOT NULL,
    "prefij" VARCHAR(2) NOT NULL,
    "coruni" NUMERIC(8,0) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- causuuni
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "causuuni" CASCADE;

CREATE TABLE "causuuni"
(
    "loguse" VARCHAR(50) NOT NULL,
    "codcat" VARCHAR(32) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

-----------------------------------------------------------------------
-- cadefcen
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefcen" CASCADE;

CREATE TABLE "cadefcen"
(
    "codcen" VARCHAR(4) NOT NULL,
    "descen" VARCHAR(250) NOT NULL,
    "dircen" VARCHAR(500),
    "codpai" VARCHAR(4),
    "nomemp" VARCHAR(250),
    "nomcar" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefcen"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefcenaco
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefcenaco" CASCADE;

CREATE TABLE "cadefcenaco"
(
    "codcenaco" VARCHAR(4) NOT NULL,
    "descenaco" VARCHAR(250) NOT NULL,
    "codpai" VARCHAR(4),
    "codedo" VARCHAR(4),
    "codciu" VARCHAR(4),
    "codmun" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefcenaco"."codpai" IS 'Código del país donde reside el organismo.';

COMMENT ON COLUMN "cadefcenaco"."codedo" IS 'Código del estado donde reside el organismo.';

COMMENT ON COLUMN "cadefcenaco"."codciu" IS 'Código del ciudad donde reside el organismo.';

COMMENT ON COLUMN "cadefcenaco"."codmun" IS 'Código del municipio donde reside el organismo.';

COMMENT ON COLUMN "cadefcenaco"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- carelartsiga
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "carelartsiga" CASCADE;

CREATE TABLE "carelartsiga"
(
    "codart" VARCHAR(20) NOT NULL,
    "codartq" VARCHAR(100) NOT NULL,
    "desartq" VARCHAR(1500) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "carelartsiga"."codart" IS 'Código del articulo del siga';

COMMENT ON COLUMN "carelartsiga"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cacontxtalm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cacontxtalm" CASCADE;

CREATE TABLE "cacontxtalm"
(
    "codalm" VARCHAR(20) NOT NULL,
    "iniart" NUMERIC(3,0) NOT NULL,
    "finart" NUMERIC(3,0) NOT NULL,
    "inides" NUMERIC(3,0) NOT NULL,
    "findes" NUMERIC(3,0) NOT NULL,
    "inican" NUMERIC(3,0) NOT NULL,
    "fincan" NUMERIC(3,0) NOT NULL,
    "inisub" NUMERIC(3,0) NOT NULL,
    "finsub" NUMERIC(3,0) NOT NULL,
    "iniiva" NUMERIC(3,0) NOT NULL,
    "finiva" NUMERIC(3,0) NOT NULL,
    "inipre" NUMERIC(3,0) NOT NULL,
    "finpre" NUMERIC(3,0) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cacontxtalm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caequiart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caequiart" CASCADE;

CREATE TABLE "caequiart"
(
    "codqpr" VARCHAR(20) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caequiart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- camigtxtven
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "camigtxtven" CASCADE;

CREATE TABLE "camigtxtven"
(
    "codalm" VARCHAR(20) NOT NULL,
    "fecven" DATE,
    "codart" VARCHAR(20) NOT NULL,
    "desart" VARCHAR(1500) NOT NULL,
    "cantidad" NUMERIC(14,2),
    "subtot" NUMERIC(14,2),
    "iva" NUMERIC(14,2),
    "precio" NUMERIC(14,2),
    "fecmig" DATE,
    "usumig" VARCHAR(250),
    "md5arch" VARCHAR(250),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "camigtxtven"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caunialart
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caunialart" CASCADE;

CREATE TABLE "caunialart"
(
    "codart" VARCHAR(20) NOT NULL,
    "unialt" VARCHAR(15),
    "relart" VARCHAR(25),
    "codunimed" VARCHAR(6),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caunialart"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catipalmpv
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catipalmpv" CASCADE;

CREATE TABLE "catipalmpv"
(
    "codtippv" VARCHAR(3) NOT NULL,
    "nomtippv" VARCHAR(100) NOT NULL,
    "metdes" NUMERIC(14,2),
    "methas" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "catipalmpv"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caconcla
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caconcla" CASCADE;

CREATE TABLE "caconcla"
(
    "ordcon" VARCHAR(8) NOT NULL,
    "descla" VARCHAR(3000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caconcla"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefcla
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefcla" CASCADE;

CREATE TABLE "cadefcla"
(
    "codcla" VARCHAR(4) NOT NULL,
    "descla" VARCHAR(2500),
    "tipcla" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefcla"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cagrucla
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cagrucla" CASCADE;

CREATE TABLE "cagrucla"
(
    "codgru" VARCHAR(4) NOT NULL,
    "desgru" VARCHAR(100),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cagrucla"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caclagru
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caclagru" CASCADE;

CREATE TABLE "caclagru"
(
    "codgru" VARCHAR(4),
    "codcla" VARCHAR(4),
    "codins" VARCHAR(4),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caclagru"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cafiacon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cafiacon" CASCADE;

CREATE TABLE "cafiacon"
(
    "ordcon" VARCHAR(8) NOT NULL,
    "fecfia" DATE,
    "insfin" VARCHAR(50),
    "tipfia" VARCHAR(50),
    "numfia" VARCHAR(50),
    "monfia" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cafiacon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cacrocon
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cacrocon" CASCADE;

CREATE TABLE "cacrocon"
(
    "ordcon" VARCHAR(8) NOT NULL,
    "fecpag" DATE,
    "nropag" VARCHAR(10),
    "monpag" NUMERIC(14,2),
    "pormon" NUMERIC(14,2),
    "poramo" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cacrocon"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caentord
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caentord" CASCADE;

CREATE TABLE "caentord"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codalm" VARCHAR(20) NOT NULL,
    "canent" NUMERIC(14,2) NOT NULL,
    "canrec" NUMERIC(14,2),
    "fecent" DATE NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caentord"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- causualm
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "causualm" CASCADE;

CREATE TABLE "causualm"
(
    "cedemp" VARCHAR(16) NOT NULL,
    "codalm" VARCHAR(20) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "causualm"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cacotdisrgo
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cacotdisrgo" CASCADE;

CREATE TABLE "cacotdisrgo"
(
    "refcot" VARCHAR(8) NOT NULL,
    "codcat" VARCHAR(50) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "codrgo" VARCHAR(4) NOT NULL,
    "monrgo" NUMERIC(14,4),
    "tipdoc" VARCHAR(4),
    "codpre" VARCHAR(50),
    "tipo" VARCHAR(1),
    "desart" VARCHAR(2000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cacotdisrgo"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caentpda
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caentpda" CASCADE;

CREATE TABLE "caentpda"
(
    "ordcom" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "desart" VARCHAR(1000),
    "canart" NUMERIC(14,2) NOT NULL,
    "fecent" DATE NOT NULL,
    "tiptra" VARCHAR(3),
    "dirori" VARCHAR(1000),
    "observ" VARCHAR(500),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "caentpda"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caartalt
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caartalt" CASCADE;

CREATE TABLE "caartalt"
(
    "codart" VARCHAR(20) NOT NULL,
    "codalt" VARCHAR(20) NOT NULL,
    "actcod" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caartalt" IS 'Tabla que contiene la información de los artículos alternos';

COMMENT ON COLUMN "caartalt"."codart" IS 'Código del Artículo';

COMMENT ON COLUMN "caartalt"."codalt" IS 'Código Alterno';

COMMENT ON COLUMN "caartalt"."actcod" IS 'Activo (S-N)';

COMMENT ON COLUMN "caartalt"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caregiones
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caregiones" CASCADE;

CREATE TABLE "caregiones"
(
    "codreg" VARCHAR(4) NOT NULL,
    "nomreg" VARCHAR(500) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caregiones" IS 'Tabla que contiene la información de las regiones';

COMMENT ON COLUMN "caregiones"."codreg" IS 'Código de la región';

COMMENT ON COLUMN "caregiones"."nomreg" IS 'Nombre de la región';

COMMENT ON COLUMN "caregiones"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caregest
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caregest" CASCADE;

CREATE TABLE "caregest"
(
    "codreg" VARCHAR(4) NOT NULL,
    "codedo" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caregest" IS 'Tabla que contiene la información de las estados por regiones';

COMMENT ON COLUMN "caregest"."codreg" IS 'Código de la región';

COMMENT ON COLUMN "caregest"."codedo" IS 'Código del estado';

COMMENT ON COLUMN "caregest"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- catiptrans
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "catiptrans" CASCADE;

CREATE TABLE "catiptrans"
(
    "codtrans" VARCHAR(3) NOT NULL,
    "destrans" VARCHAR(500) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "catiptrans" IS 'Tabla que contiene la información de los Tipos de Transporte';

COMMENT ON COLUMN "catiptrans"."codtrans" IS 'Código del Tipo de Transporte';

COMMENT ON COLUMN "catiptrans"."destrans" IS 'Nombre del Tipo de Transporte';

COMMENT ON COLUMN "catiptrans"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- capdaoc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "capdaoc" CASCADE;

CREATE TABLE "capdaoc"
(
    "refpda" VARCHAR(15) NOT NULL,
    "fecpda" DATE NOT NULL,
    "despda" VARCHAR(2000),
    "monpda" NUMERIC(14,2),
    "stapad" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "capdaoc" IS 'Tabla que contiene información referente a Plan de Abastecimiento PDA';

COMMENT ON COLUMN "capdaoc"."refpda" IS 'Código del PDA.';

COMMENT ON COLUMN "capdaoc"."fecpda" IS 'Fecha del PDA.';

COMMENT ON COLUMN "capdaoc"."despda" IS 'Descripción del PDA.';

COMMENT ON COLUMN "capdaoc"."monpda" IS 'Monto del PDA.';

COMMENT ON COLUMN "capdaoc"."stapad" IS 'Estatus del PDA.';

COMMENT ON COLUMN "capdaoc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetpda
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetpda" CASCADE;

CREATE TABLE "cadetpda"
(
    "refpda" VARCHAR(15) NOT NULL,
    "ordcom" VARCHAR(8) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "desart" VARCHAR(1000),
    "canart" NUMERIC(14,2) NOT NULL,
    "fecent" DATE NOT NULL,
    "tiptra" VARCHAR(3),
    "dirori" VARCHAR(1000) NOT NULL,
    "codedo" VARCHAR(4) NOT NULL,
    "catipalm_id" INTEGER NOT NULL,
    "tmart" NUMERIC(14,2),
    "codpro" VARCHAR(15) NOT NULL,
    "observ" VARCHAR(1000),
    "mes" VARCHAR(15) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cadetpda" IS 'Tabla que contiene información referente al detalle del PDA';

COMMENT ON COLUMN "cadetpda"."refpda" IS 'Código del PDA.';

COMMENT ON COLUMN "cadetpda"."ordcom" IS 'Nro del la Orden de Compra.';

COMMENT ON COLUMN "cadetpda"."codart" IS 'Código del Artículo.';

COMMENT ON COLUMN "cadetpda"."desart" IS 'Descripción del Artículo.';

COMMENT ON COLUMN "cadetpda"."canart" IS 'Cantidad de Artículo.';

COMMENT ON COLUMN "cadetpda"."fecent" IS 'Fecha de Entrega.';

COMMENT ON COLUMN "cadetpda"."tiptra" IS 'Tipo de Transporte.';

COMMENT ON COLUMN "cadetpda"."dirori" IS 'Dirección de Origen.';

COMMENT ON COLUMN "cadetpda"."codedo" IS 'Código de estado';

COMMENT ON COLUMN "cadetpda"."catipalm_id" IS 'Tipo de Almacén.';

COMMENT ON COLUMN "cadetpda"."tmart" IS 'Toneladas Métricas del Artículo.';

COMMENT ON COLUMN "cadetpda"."codpro" IS 'Código del Proveedor.';

COMMENT ON COLUMN "cadetpda"."observ" IS 'Observaciones adicionales';

COMMENT ON COLUMN "cadetpda"."mes" IS 'MES';

COMMENT ON COLUMN "cadetpda"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caalmpda
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caalmpda" CASCADE;

CREATE TABLE "caalmpda"
(
    "refpda" VARCHAR(15) NOT NULL,
    "codart" VARCHAR(20) NOT NULL,
    "id_reg" INTEGER NOT NULL,
    "codalm" VARCHAR(20) NOT NULL,
    "codubi" VARCHAR(20),
    "candis" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caalmpda" IS 'Tabla que contiene información referente a los la distribucion en almacen de PDA';

COMMENT ON COLUMN "caalmpda"."refpda" IS 'Código del PDA.';

COMMENT ON COLUMN "caalmpda"."codart" IS 'Código del Artículo.';

COMMENT ON COLUMN "caalmpda"."codalm" IS 'Código del Almacén donde esta el Articulo.';

COMMENT ON COLUMN "caalmpda"."codubi" IS 'Código de Ubicación del Articulo.';

COMMENT ON COLUMN "caalmpda"."candis" IS 'Cantidad distribuida en ese almacén y ubicación';

COMMENT ON COLUMN "caalmpda"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caunicen
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caunicen" CASCADE;

CREATE TABLE "caunicen"
(
    "codcat" VARCHAR(32),
    "codcen" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caunicen" IS 'Asociación de Centros de Costo a Unidades';

COMMENT ON COLUMN "caunicen"."codcat" IS 'Unidad Administrativa.';

COMMENT ON COLUMN "caunicen"."codcen" IS 'Centro de Costo';

COMMENT ON COLUMN "caunicen"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefunimed
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefunimed" CASCADE;

CREATE TABLE "cadefunimed"
(
    "codunimed" VARCHAR(6) NOT NULL,
    "desunimed" VARCHAR(50) NOT NULL,
    "abrunimed" VARCHAR(50),
    "tipunimed" VARCHAR(1),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefunimed"."tipunimed" IS 'Tipo de Unidad de Medida';

COMMENT ON COLUMN "cadefunimed"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefdirec
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefdirec" CASCADE;

CREATE TABLE "cadefdirec"
(
    "coddirec" VARCHAR(4) NOT NULL,
    "desdirec" VARCHAR(500) NOT NULL,
    "codcat" VARCHAR(32),
    "escentral" BOOLEAN DEFAULT 'f',
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefdirec"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cadefdirec"."desdirec" IS 'Descripción de la Dirección';

COMMENT ON COLUMN "cadefdirec"."codcat" IS 'Categoria Presupuestaria';

COMMENT ON COLUMN "cadefdirec"."escentral" IS 'Indica si es Central';

COMMENT ON COLUMN "cadefdirec"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefdivi
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefdivi" CASCADE;

CREATE TABLE "cadefdivi"
(
    "coddivi" VARCHAR(6) NOT NULL,
    "desdivi" VARCHAR(500) NOT NULL,
    "coddirec" VARCHAR(4) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON COLUMN "cadefdivi"."coddivi" IS 'Código de la División';

COMMENT ON COLUMN "cadefdivi"."desdivi" IS 'Descripción de la División';

COMMENT ON COLUMN "cadefdivi"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cadefdivi"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- casolcot
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "casolcot" CASCADE;

CREATE TABLE "casolcot"
(
    "numsolcot" VARCHAR(8) NOT NULL,
    "fecsolcot" DATE NOT NULL,
    "reqart" VARCHAR(8) NOT NULL,
    "dessolcot" VARCHAR(1000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "casolcot" IS 'Tabla para registrar los datos de la Solicitud de cotización';

COMMENT ON COLUMN "casolcot"."numsolcot" IS 'Número de Solicitud de Cotización';

COMMENT ON COLUMN "casolcot"."fecsolcot" IS 'Fecha de Solicitud de Cotización';

COMMENT ON COLUMN "casolcot"."reqart" IS 'Número de Solicitud de Egresos';

COMMENT ON COLUMN "casolcot"."dessolcot" IS 'Descripción de Solicitud de Cotización';

COMMENT ON COLUMN "casolcot"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetsolcot
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetsolcot" CASCADE;

CREATE TABLE "cadetsolcot"
(
    "numsolcot" VARCHAR(8) NOT NULL,
    "codpro" VARCHAR(15) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cadetsolcot" IS 'Tabla para registrar los provedores que cotizaran para una SC';

COMMENT ON COLUMN "cadetsolcot"."numsolcot" IS 'Número de Solicitud de Cotización';

COMMENT ON COLUMN "cadetsolcot"."codpro" IS 'Código del Proveedor';

COMMENT ON COLUMN "cadetsolcot"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cafirdocpre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cafirdocpre" CASCADE;

CREATE TABLE "cafirdocpre"
(
    "coddirec" VARCHAR(4) NOT NULL,
    "tipdoc" VARCHAR(2) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cafirdocpre" IS 'Tabla para configuración de  firmas para  documentos preimpresos';

COMMENT ON COLUMN "cafirdocpre"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cafirdocpre"."tipdoc" IS 'Tipo de Documento';

COMMENT ON COLUMN "cafirdocpre"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadetfirdocpre
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadetfirdocpre" CASCADE;

CREATE TABLE "cadetfirdocpre"
(
    "coddirec" VARCHAR(4) NOT NULL,
    "tipdoc" VARCHAR(2) NOT NULL,
    "numfir" INTEGER,
    "nomfir" VARCHAR(500),
    "carfir" VARCHAR(500),
    "obsfir" VARCHAR(1000),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cadetfirdocpre" IS 'Tabla para el detalle de  firmas para  documentos preimpresos';

COMMENT ON COLUMN "cadetfirdocpre"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "cadetfirdocpre"."tipdoc" IS 'Tipo de Documento';

COMMENT ON COLUMN "cadetfirdocpre"."numfir" IS 'numero de la firma';

COMMENT ON COLUMN "cadetfirdocpre"."nomfir" IS 'Nombre';

COMMENT ON COLUMN "cadetfirdocpre"."carfir" IS 'Cargo';

COMMENT ON COLUMN "cadetfirdocpre"."obsfir" IS 'Observación';

COMMENT ON COLUMN "cadetfirdocpre"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caunitridir
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caunitridir" CASCADE;

CREATE TABLE "caunitridir"
(
    "coddirec" VARCHAR(4) NOT NULL,
    "fecvig" DATE NOT NULL,
    "canunitri" INTEGER,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caunitridir" IS 'Parametrizar el Monto Máximo de las Compras en Unidades Tributarios';

COMMENT ON COLUMN "caunitridir"."coddirec" IS 'Código de la Dirección';

COMMENT ON COLUMN "caunitridir"."fecvig" IS 'Fecha de Vigencia';

COMMENT ON COLUMN "caunitridir"."canunitri" IS 'Cantidad de Unidad de Tributaria';

COMMENT ON COLUMN "caunitridir"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- caporressoc
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "caporressoc" CASCADE;

CREATE TABLE "caporressoc"
(
    "mondes" NUMERIC(14,2),
    "monhas" NUMERIC(14,2),
    "porres" NUMERIC(14,2),
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "caporressoc" IS 'Tabla que contiene información referente a los Porcentaje de Responsabilidad Social';

COMMENT ON COLUMN "caporressoc"."mondes" IS 'Monto de Orden Desde';

COMMENT ON COLUMN "caporressoc"."monhas" IS 'Monto de Orden Hasta';

COMMENT ON COLUMN "caporressoc"."porres" IS 'Porcentaje de Responsabilidad Social';

COMMENT ON COLUMN "caporressoc"."id" IS 'Identificador Único del registro';

-----------------------------------------------------------------------
-- cadefgar
-----------------------------------------------------------------------

DROP TABLE IF EXISTS "cadefgar" CASCADE;

CREATE TABLE "cadefgar"
(
    "codgar" VARCHAR(3) NOT NULL,
    "desgar" VARCHAR(100) NOT NULL,
    "id" INTEGER NOT NULL,
    PRIMARY KEY ("id")
);

COMMENT ON TABLE "cadefgar" IS 'Tabla para guardar los tipos de garantías';

COMMENT ON COLUMN "cadefgar"."codgar" IS 'Código de la Garantía';

COMMENT ON COLUMN "cadefgar"."desgar" IS 'Descripción de la Garantía';

COMMENT ON COLUMN "cadefgar"."id" IS 'Identificador Único del registro';

ALTER TABLE "cadefalm" ADD CONSTRAINT "cadefalm_FK_1"
    FOREIGN KEY ("codtip")
    REFERENCES "catipalm" ("id");

ALTER TABLE "cadefalm" ADD CONSTRAINT "cadefalm_FK_2"
    FOREIGN KEY ("catipalm_id")
    REFERENCES "catipalm" ("id");

ALTER TABLE "cadettra" ADD CONSTRAINT "cadettra_FK_1"
    FOREIGN KEY ("codfal")
    REFERENCES "camotfal" ("codfal");

ALTER TABLE "cadphart" ADD CONSTRAINT "cadphart_FK_1"
    FOREIGN KEY ("codalmusu")
    REFERENCES "cadefalm" ("codalm");

ALTER TABLE "carcpart" ADD CONSTRAINT "carcpart_FK_1"
    FOREIGN KEY ("codalmusu")
    REFERENCES "cadefalm" ("codalm");

ALTER TABLE "carecaud" ADD CONSTRAINT "carecaud_FK_1"
    FOREIGN KEY ("codtiprec")
    REFERENCES "catiprec" ("codtiprec");

ALTER TABLE "carecpro" ADD CONSTRAINT "carecpro_FK_1"
    FOREIGN KEY ("codpro")
    REFERENCES "caprovee" ("codpro");

ALTER TABLE "carecpro" ADD CONSTRAINT "carecpro_FK_2"
    FOREIGN KEY ("codrec")
    REFERENCES "carecaud" ("codrec");

ALTER TABLE "cargosol" ADD CONSTRAINT "cargosol_FK_1"
    FOREIGN KEY ("codrgo")
    REFERENCES "carecarg" ("codrgo");

ALTER TABLE "casolart" ADD CONSTRAINT "casolart_FK_1"
    FOREIGN KEY ("tipmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "caordcom" ADD CONSTRAINT "caordcom_FK_1"
    FOREIGN KEY ("codpro")
    REFERENCES "caprovee" ("codpro");

ALTER TABLE "caordcom" ADD CONSTRAINT "caordcom_FK_2"
    FOREIGN KEY ("conpag")
    REFERENCES "caconpag" ("codconpag");

ALTER TABLE "caordcom" ADD CONSTRAINT "caordcom_FK_3"
    FOREIGN KEY ("forent")
    REFERENCES "caforent" ("codforent");

ALTER TABLE "caordcom" ADD CONSTRAINT "caordcom_FK_4"
    FOREIGN KEY ("tipmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "cacotiza" ADD CONSTRAINT "cacotiza_FK_1"
    FOREIGN KEY ("codpro")
    REFERENCES "caprovee" ("codpro");

ALTER TABLE "cacotiza" ADD CONSTRAINT "cacotiza_FK_2"
    FOREIGN KEY ("tipmon")
    REFERENCES "tsdefmon" ("codmon");

ALTER TABLE "caartalmubi" ADD CONSTRAINT "caartalmubi_FK_1"
    FOREIGN KEY ("codubi")
    REFERENCES "cadefubi" ("codubi");

ALTER TABLE "caalmubi" ADD CONSTRAINT "caalmubi_FK_1"
    FOREIGN KEY ("codubi")
    REFERENCES "cadefubi" ("codubi");

ALTER TABLE "caentalm" ADD CONSTRAINT "caentalm_FK_1"
    FOREIGN KEY ("tipmov")
    REFERENCES "catipent" ("codtipent");

ALTER TABLE "caentalm" ADD CONSTRAINT "caentalm_FK_2"
    FOREIGN KEY ("codalmusu")
    REFERENCES "cadefalm" ("codalm");

ALTER TABLE "casalalm" ADD CONSTRAINT "casalalm_FK_1"
    FOREIGN KEY ("tipmov")
    REFERENCES "catipsal" ("codtipsal");

ALTER TABLE "casalalm" ADD CONSTRAINT "casalalm_FK_2"
    FOREIGN KEY ("codalmusu")
    REFERENCES "cadefalm" ("codalm");

ALTER TABLE "catraalm" ADD CONSTRAINT "catraalm_FK_1"
    FOREIGN KEY ("codemptra")
    REFERENCES "faemptra" ("codemptra");

ALTER TABLE "catraalm" ADD CONSTRAINT "catraalm_FK_2"
    FOREIGN KEY ("fadefveh_id")
    REFERENCES "fadefveh" ("id");

ALTER TABLE "catraalm" ADD CONSTRAINT "catraalm_FK_3"
    FOREIGN KEY ("fadefcho_id")
    REFERENCES "fadefcho" ("id");

ALTER TABLE "casolraz" ADD CONSTRAINT "casolraz_FK_1"
    FOREIGN KEY ("codrazcom")
    REFERENCES "carazcom" ("codrazcom");

ALTER TABLE "caartrcp" ADD CONSTRAINT "caartrcp_FK_1"
    FOREIGN KEY ("codfal")
    REFERENCES "camotfal" ("codfal");

ALTER TABLE "carelartsiga" ADD CONSTRAINT "carelartsiga_FK_1"
    FOREIGN KEY ("codart")
    REFERENCES "caregart" ("codart");

ALTER TABLE "causualm" ADD CONSTRAINT "causualm_FK_1"
    FOREIGN KEY ("codalm")
    REFERENCES "cadefalm" ("codalm");

ALTER TABLE "cadetpda" ADD CONSTRAINT "cadetpda_FK_1"
    FOREIGN KEY ("catipalm_id")
    REFERENCES "catipalm" ("id");

ALTER TABLE "cadetpda" ADD CONSTRAINT "cadetpda_FK_2"
    FOREIGN KEY ("codpro")
    REFERENCES "caprovee" ("codpro");
